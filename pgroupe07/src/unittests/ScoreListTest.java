package unittests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.Deck;
import model.Score;
import model.ScoreList;

public class ScoreListTest {
	
	private ScoreList s1, s2;
	@Before
	public void setUp() throws Exception 
	{
		s1 = ScoreList.getInstance();
	}
	
	@Test
	public void testSingleton()
	{
		s1 = ScoreList.getInstance();
		s2 = ScoreList.getInstance();
		assertEquals(s1, s2);
	}
	
	@Test
	public void testAddScore()
	{
		Score s = new Score("user", 12, "09/05/2016 23:59:02", "Alakazam");
		int size = s1.getScores().size();
		s1.addScore(s);
		int newsize = s1.getScores().size();
		assertTrue(s1.getScores().contains(s));
		assertTrue(size+1 == newsize);
	}
	
	@Test
	public void testDelScore()
	{
		Score s = new Score("user", 12, "09/05/2016 23:59:02", "Alakazam");
		s1.addScore(s);
		s1.delScore(s);
		assertFalse(s1.getScores().contains(s));
	}
}
