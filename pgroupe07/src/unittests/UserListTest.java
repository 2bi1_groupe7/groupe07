package unittests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import model.User;
import model.UserList;

public class UserListTest {
	UserList userList;
	@Before
	public void setUp() throws Exception {
		userList=UserList.getInstance();
	}
	
	@After 
	public void tearDown()throws Exception{
		userList.getUserlist().remove(userList.getUserlist().size()-1);
	}
	@Test
	public void testAddUser() {
		int i =userList.getUserlist().size();
		User u1= new User("bob","alice");
		userList.addUser(u1);
		assertEquals(i+1,userList.getUserlist().size());
	}

}
