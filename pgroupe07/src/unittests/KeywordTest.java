package unittests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.glass.ui.Size;

import model.Keyword;
import model.Specialities;

public class KeywordTest {
	private Keyword k1,k2;
	String mot1,mot2;

	@Before
	public void setUp() throws Exception {
		mot1="salade";
		mot2="salades";
		k1=Keyword.getInstance();
		k2=Keyword.getInstance();
	}

	@Test
	public void testSingleton() {
		assertEquals(k1, k2);
	}
	@Test
	public void testAddKeyword() {
		k1.addKeyword(mot1, Specialities.MOM);
		assertTrue(k1.contains(mot1, Specialities.MOM));
	}
	@Test
	public void testDeleteKeywordWord(){
		k1.addKeyword(mot1, Specialities.MOM);
		k1.delKeyword(mot1, Specialities.MOM);
		assertFalse(k1.contains(mot1, Specialities.MOM));
	}
	@Test
	public void testDeleteKeywordIndex(){
		k1.addKeyword(mot1, Specialities.MOM);
		k1.delKeyword(0,k1.getWords().get(0).size()-1);
		assertFalse(k1.contains(mot1, Specialities.MOM));
	}
	@Test
	public void testUpdateKeyword(){
		k1.addKeyword(mot1, Specialities.MOM);
		k1.updateKeyword(mot2, 0, k1.getWords().get(0).size()-1);
		assertEquals("salades",k1.get(k1.getWords().get(0).size()-1, 0));
	}
	@Test
	public void testCheckQuestion1(){
		k1.addKeyword(mot1, Specialities.MOM);
		String question = "Je veux de la salade!";
		assertTrue(k1.checkQuestion(question, Specialities.MOM));
	}
	@Test
	public void testCheckQuestion2(){
		k1.addKeyword(mot1, Specialities.MOM);
		String question = "Je veux de la!";
		assertFalse(k1.checkQuestion(question, Specialities.MOM));
	}
	@Test
	public void testGetKeyword(){
		k1.addKeyword(mot1, Specialities.MOM);
		assertEquals(mot1,k1.get(k1.getWords().get(0).size()-1, 0));
	}
}
