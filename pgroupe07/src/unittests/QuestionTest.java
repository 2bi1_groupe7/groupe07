package unittests;

import static org.junit.Assert.*;

import java.util.HashMap;

import model.Question;
import model.Round;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import exceptions.CheckFailException;
import exceptions.DuplicateChoiceException;
import exceptions.MoreThan4ChoicesException;


public class QuestionTest {
	private Question question;
	 

	
	@Before
	public void setUp() throws Exception {
		question = new Question();
		
	}

	@After
	public void tearDown() throws Exception {
		question = null;
	}

	@Test
	public void testAddChoices() throws DuplicateChoiceException, MoreThan4ChoicesException {
		question.addChoice("Bruxelles", true);
		assertTrue("Ajout normal",question.getChoices().containsKey("Bruxelles"));
		
	}
	
	@Test(expected=MoreThan4ChoicesException.class)
	public void testMoreThan4Choices() throws Exception{
		question.addChoice("Bruxelles", true);
		question.addChoice("Vienne", false);
		question.addChoice("Paris", false);
		question.addChoice("Luxembourg", false);
		question.addChoice("Mons", false);
		assertEquals("Ajout de 4 choix",question.getChoices().size(),4);
	}
	
	@Test(expected=DuplicateChoiceException.class)
	public void testDuplicateChoice() throws Exception{
		question.addChoice("Bruxelles", true);
		question.addChoice("Bruxelles", true);
		assertEquals("Ajout de 4 choix",question.getChoices().size(),1);
	}
	
	@Test(expected=CheckFailException.class)
	public void testCheckQuestion() throws Exception{
		question.setAuthor("Roger");
		question.setRound(Round.SECOND_ROUND);
		question.setStatement("Quelle est la capitale de la Belgique ?");
		question.addChoice("Bruxelles", true);
		question.addChoice("Vienne", false);
		question.addChoice("Paris", false);
		question.addChoice("Luxembourg", true);
		assertEquals("Check de la question",question.check(),false);
	}
	
	@Test
	public void testDeleteChoice() throws DuplicateChoiceException, MoreThan4ChoicesException{
		question.addChoice("Bruxelles", true);
		question.deleteChoice("Bruxelles");
		assertEquals("Ajout de 4 choix",question.getChoices().size(),0);
	}
	
	@Test
	public void testToJson() throws DuplicateChoiceException, MoreThan4ChoicesException{
		question.setAuthor("Potter");
		question.setRound(Round.FIRST_ROUND);
		question.setStatement("Quelle est la capitale de l'Equateur?");
		question.addChoice("Caracas", false);
		question.addChoice("Lima", false);
		question.addChoice("Quito", true);
		question.addChoice("Bogota", false);
		String tmp=question.toJson();
		String tempo="{\"author\":\"Potter\",\"statement\":\"Quelle est la capitale de l\\u0027Equateur?\",\"round\":\"FIRST_ROUND\",\"choices\":{\"Caracas\":false,\"Lima\":false,\"Quito\":true,\"Bogota\":false}}";
		assertEquals("toJson",tmp,tempo);
	}
	
	@Test
	public void testFromJson() throws DuplicateChoiceException, MoreThan4ChoicesException{
		question.setAuthor("Potter");
		question.setRound(Round.FIRST_ROUND);
		question.setStatement("Quelle est la capitale de l\u0027Equateur?");
		question.addChoice("Caracas", false);
		question.addChoice("Lima", false);
		question.addChoice("Quito", true);
		question.addChoice("Bogota", false);
		String tmp="{\"author\":\"Potter\",\"statement\":\"Quelle est la capitale de l\u0027Equateur?\",\"round\":\"FIRST_ROUND\",\"choices\":{\"Caracas\": false,\"Lima\": false,\"Quito\": true,\"Bogota\": false}}";
		Question q1 = new Question() ;
		q1 = Question.fromJson(tmp);
		assertEquals("FromJson",question,q1);
		assertEquals("FromJson",question.getAuthor(),q1.getAuthor());
		assertEquals("FromJson",question.getRound(),q1.getRound());
		assertEquals("FromJson",question.getChoices(),q1.getChoices());
	}
	@Test
	public void testClone() throws DuplicateChoiceException, MoreThan4ChoicesException{
		question.setAuthor("Roger");
		question.setRound(Round.SECOND_ROUND);
		question.setStatement("Quelle est la capitale de la Belgique ?");
		question.addChoice("Bruxelles", true);
		question.addChoice("Vienne", false);
		question.addChoice("Paris", false);
		question.addChoice("Luxembourg", true);
		Question tmp=(Question)question.clone();
		System.out.println(tmp.getStatement());
		assertTrue(question.equals(tmp));
	}
	@Test
	public void testCheckAnswerChar() throws DuplicateChoiceException, MoreThan4ChoicesException{
		int i=0,tmp=0;
		question.setRound(Round.SECOND_ROUND);
		question.setStatement("Quelle est la capitale de la Belgique ?");
		question.addChoice("Bruxelles", true);
		question.addChoice("Vienne", false);
		question.addChoice("Paris", false);
		question.addChoice("Luxembourg", true);
		for(String str : question.getChoices().keySet()){
			if(question.getChoices().get(str).equals(true)){
				tmp=i;
			}
			i++;
		}
		char test =(char)('A'+tmp);
		assertTrue(question.checkAnswer(test));
	}
	@Test
	public void testGoodQuestion() throws DuplicateChoiceException, MoreThan4ChoicesException{
		question.setRound(Round.SECOND_ROUND);
		question.setStatement("Quelle est la capitale de la Belgique ?");
		question.addChoice("Bruxelles", true);
		question.addChoice("Vienne", false);
		question.addChoice("Paris", false);
		question.addChoice("Luxembourg", false);
		System.out.println(question.goodQuestion());
		assertEquals(question.goodQuestion(),"Bruxelles");
	}
}
