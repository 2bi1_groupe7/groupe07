package unittests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;

import model.Deck;
import model.Question;
import model.Round;

import org.junit.Before;
import org.junit.Test;

import exceptions.CheckFailException;
import exceptions.DeckException;
import exceptions.MoreThan4ChoicesException;
import exceptions.QuestionException;

public class DeckTest 
{
	private Deck d1=Deck.getInstance(), d2=Deck.getInstance();
	private Question q, q2, q3;
	/**
	 * Sets Up before tests.
	 * 
	 */
	@Before
	public void setUp()
	{
		HashMap<String, Boolean> hm = new HashMap<String, Boolean>();
		HashMap<String, Boolean> hm2 = new HashMap<String, Boolean>();
		HashMap<String, Boolean> hm3 = new HashMap<String, Boolean>();
		
		hm.put("Hano�", false);
		hm.put("Thimphou", true);
		hm.put("Vientiane", false);
		hm.put("Beijing", false);
		
		hm3.put("Hano�", true);
		hm3.put("Thimphou", false);
		hm3.put("Vientiane", false);
		hm3.put("Beijing", false);
		
		q = new Question("Lipani Corrado", Round.LAST_ROUND, "Quelle est la capitale du Bhoutan ?", hm);
		q2 = new Question("Lipani Corrado", Round.SECOND_ROUND, "Quelle est la capitale du Bhoutan ?", hm2);
		q3 = new Question("Lipani Corrado", Round.SECOND_ROUND, "Quelle est la capitale du Vietnam ?", hm3);
	}

	/**
	 * Checks if there is in fact only one instance for Deck.
	 * 
	 */
	@Test
	public void testSingleton() 
	{
		d1 = Deck.getInstance();
		d2 = Deck.getInstance();
		assertEquals(d1, d2);
	}
	
	@Test
	public void testAddQuestion() throws QuestionException, CheckFailException, MoreThan4ChoicesException
	{
		d1.addQuestion(q);
		assertTrue(d1.getQuestions().contains(q));
	}
	
	
	@Test
	public void testDeleteQuestion() throws QuestionException, CheckFailException, MoreThan4ChoicesException
	{
		int size = d1.getQuestions().size();
		d1.addQuestion(q);
		size++;
		d1.deleteQuestion(q);
		assertTrue(d1.getQuestions().size() == size-1);
	}
	
	@Test
	public void testIsDuplicate() throws QuestionException, CheckFailException, MoreThan4ChoicesException
	{
		d1.addQuestion(q);
		assertTrue(d1.isDuplicate(q2));
		assertFalse(d1.isDuplicate(q3));
	}
	
	@Test(expected=DeckException.class)
	public void testUpdateQuestionExceptionDeck() throws DeckException, QuestionException, CheckFailException, MoreThan4ChoicesException
	{
		d1.updateQuestion(q3, q);
	}
	
	
	@Test
	public void testUpdateQuestion() throws DeckException, QuestionException, CheckFailException, MoreThan4ChoicesException
	{
		d1.addQuestion(q3);
		d1.updateQuestion(q3, q);
	}
	@Test
	public void testGetRound1(){
		ArrayList<Question> q=d1.getRound1();
		boolean test=true;
		for(Question tmp:q){
			if(tmp.getRound()!=Round.FIRST_ROUND){
				test=false;
			}
		}
		assertTrue(test);
	}
	@Test
	public void testGetRound2(){
		ArrayList<Question> q=d1.getRound2();
		boolean test=true;
		for(Question tmp:q){
			if(tmp.getRound()!=Round.SECOND_ROUND){
				test=false;
			}
		}
		assertTrue(test);
	}
	@Test
	public void testGetRound3(){
		ArrayList<Question> q=d1.getRound3();
		boolean test=true;
		for(Question tmp:q){
			if(tmp.getRound()!=Round.LAST_ROUND){
				test=false;
			}
		}
		assertTrue(test);
	}
	@Test
	public void testGetRandom1(){
		Question q=d1.getRandomQuestion(1);
		assertTrue((q.getRound()==Round.FIRST_ROUND)&&d1.isDuplicate(q));
	}
	@Test
	public void testGetRandom2(){
		Question q=d1.getRandomQuestion(7);
		assertTrue((q.getRound()==Round.SECOND_ROUND)&&d1.isDuplicate(q));
	}
	@Test
	public void testGetRandom3(){
		Question q=d1.getRandomQuestion(14);
		assertTrue((q.getRound()==Round.LAST_ROUND)&&d1.isDuplicate(q));
	}
}