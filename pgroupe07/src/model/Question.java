package model;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import exceptions.CheckFailException;
import exceptions.DuplicateChoiceException;
import exceptions.MoreThan4ChoicesException;


/**
 * the class representing a question with a level of difficulty and different answers.
 * @author De Cnuydt Benjamin
 *
 */
public class Question implements Cloneable,Comparable<Question>{
	private String author, statement;
	private Round round;
	private HashMap<String, Boolean> choices;
	
	
		
	/**
	 * Constructor with all the information for the questions
	 * @param author the author of the question
	 * @param round the level of difficulty
	 * @param statement the statement of the question
	 * @param choices the different responses
	 */
	public Question(String author, Round round, String statement, HashMap<String, Boolean> choices)
	{
		this.author = author;
		this.round = round;
		this.statement = statement;
		this.choices = choices;
	}
	
	/**
	 * default constructor. Initializes the hashmap of responses.
	 */
	public Question(){
		choices = new HashMap<String, Boolean>();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	public Object clone(){
		String cAuthor=new String(this.author);
		Round cRound = this.round;
		String cStatement= new String(this.statement);
		HashMap<String, Boolean> cChoices=new HashMap<String, Boolean>();
		String r=(String) this.choices.keySet().toArray()[0];
		cChoices.put(r,this.choices.get(r));
		r=(String) this.choices.keySet().toArray()[1];
		cChoices.put(r,this.choices.get(r));
		r=(String) this.choices.keySet().toArray()[2];
		cChoices.put(r,this.choices.get(r));
		r=(String) this.choices.keySet().toArray()[3];
		cChoices.put(r,this.choices.get(r));
		return new Question(cAuthor,cRound,cStatement,cChoices);
	}
	
	/**
	 * Checks if the question is correctly initialized
	 * @return true if the question is correctly initialized, else false;
	 * @throws CheckFailException if the question doesn't have a right answer.
	 * @throws MoreThan4ChoicesException if the question has more than 4 choices.
	 */
	public boolean check() throws CheckFailException,MoreThan4ChoicesException{
		if(choices.size()==4){
			int tmp=0;
			Iterator it = choices.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry rep = (Map.Entry)it.next();
				if((boolean)rep.getValue()){
					tmp++;
				} 
			}
			if(tmp>1 || tmp==0){
				throw new CheckFailException(this);
			}
		}
		else{
			throw new MoreThan4ChoicesException();
		}
		return true;
	}
	
	/**
	 * Adds a choice to the question.
	 * @param choice the choice.
	 * @param bool if the choice is the good answer.
	 * @throws DuplicateChoiceException if the choice already exist.
	 * @throws MoreThan4ChoicesException if the question have more than 4 choices.
	 */
	public void addChoice(String choice, Boolean bool) throws DuplicateChoiceException,MoreThan4ChoicesException
	{
		Iterator it = choices.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry rep = (Map.Entry)it.next();
			if(rep.getKey().equals(choice)){
				throw new DuplicateChoiceException();
			}
		}
		if(choices.size()>=4){
			throw new MoreThan4ChoicesException();
		}
		this.choices.put(choice, bool);
	}
	
	/**
	 * deletes a choice from the question.
	 * @param choice the choice.
	 */
	public void deleteChoice(String choice)
	{
		this.choices.remove(choice);
	}
	
	/**
	 * Checks if the character gives an answer.
	 * @param car the character
	 * @return true if the character gives an answer, else false.
	 */
	public Boolean checkAnswer(char car){
		String answer = (String)this.getChoices().keySet().toArray()[Character.toUpperCase(car)-65];
		if(this.getChoices().get(answer)){
			return true;
		}
		return false;
	}
	/**
	 * Checks if the answer sent is a real answer.
	 * @param answer the answer sent
	 * @return true if the answer sent is a real anwser, else false.
	 */
	public Boolean checkAnswer(String answer){
		if(this.getChoices().get(answer)){
			return true;
		}
		return false;
	}
	/**
	 * gives a json representation of the class
	 * @return the json representation of the class
	 */
	public String toJson()
	{
		String json;
		Gson gson = new GsonBuilder().create();
		json = gson.toJson(this);
		return json;
	}
	/**
	 * Builds a class Question from a Json
	 * @param json the json representation of the class
	 * @return a class Question.
	 */
	public static Question fromJson(String json)
	{
		Gson gson = new GsonBuilder().create();
		Question q = gson.fromJson(json, Question.class);
		return q;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o){
		if(o instanceof Question){
			Question tmp = (Question) o;
			return this.statement.equals(tmp.statement);
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return 	"AUTHOR : "+author+"\n"+"QUESTION : "+statement+"\nROUND : "+round+"\nCHOICES : "+choices.toString();
	}
	
	/**
	 * gets the author of the question
	 * @return the author of the question
	 */
	public String getAuthor() {
		return author;
	}
	
	/**
	 * Sets the new name of the author
	 * @param author the name of the new author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
	/**
	 * gets the statement of the question.
	 * @return the statement of the question.
	 */
	public String getStatement() {
		return statement;
	}
	
	/**
	 * sets the new statement of the question.
	 * @param statement the new statement of the question.
	 */
	public void setStatement(String statement) {
		this.statement = statement;
	}
	
	/**
	 * gets the round for the question.
	 * @return the round for the question.
	 */
	public Round getRound() {
		return round;
	}
	
	/**
	 * sets the new round of the question.
	 * @param round the new round of the question.
	 */
	public void setRound(Round round) {
		this.round = round;
	}
	
	/**
	 * gets the choices for the question.
	 * @return the choices for the question.
	 */
	public HashMap<String, Boolean> getChoices() {
		return choices;
	}
	
	/**
	 * sets the new choices for the question.
	 * @param choices the new choices for the question.
	 */
	public void setChoices(HashMap<String, Boolean> choices) {
		this.choices = choices;
	}
	
	/**
	 * returns the good answer of the question.
	 * @return the good answer of the question
	 */
	public String goodQuestion(){
		for(String str : this.getChoices().keySet()){
			if(this.getChoices().get(str).equals(true)){
				return str;
			}
		}
		return "";
	}
	
	/**
	 * returns a random bad answer for the question.
	 * @return a random bad answer for the question.
	 */
	public int badAnswer(){
		Random rand = new Random();
		int i=rand.nextInt(4);
		if(!this.checkAnswer((String)this.getChoices().keySet().toArray()[i])){
			return i;
		}
		return badAnswer();
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Question other) 
	{
		return this.getAuthor().compareTo(other.getAuthor());
	}
}
