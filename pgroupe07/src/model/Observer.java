package model;

/**
 * A class can implement the Observer interface when it wants to be informed of changes in observable objects.
 * @author Lipani Corrado
 *
 */
public interface Observer 
{
	/**
	 * this method is called when an object is changed.
	 */
	public void update();
}
