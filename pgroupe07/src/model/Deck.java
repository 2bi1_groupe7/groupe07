package model;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import exceptions.CheckFailException;
import exceptions.DeckException;
import exceptions.MoreThan4ChoicesException;
import exceptions.QuestionException;
import util.Serialisation;

/**
 * The class representing a deck of questions for the game.
 * 
 * @author Lipani Corrado
 *
 */
public class Deck extends Observable
{
	
	private ArrayList<Question> questions,round1,round2,round3;
	private static Deck deck = null;
	
	/**
	 * Default Constructor.
	 * 
	 */
	private Deck()
	{
		questions=new ArrayList<Question>();
	}
	
	/**
	 * Gets the only instance of the deck.
	 * 
	 * @return		the only instance of the deck.
	 */
	public static Deck getInstance()
	{
		if (deck == null)
		{
			deck =  Deck.fromJson(Serialisation.readFile("Deck.json"));
			if(deck==null){
				deck=new Deck();
			}
		}
		Collections.sort(deck.getQuestions());
		return deck;
	}
	
	/**
	 * Adds a question into the deck.
	 * 
	 * @param q		the question to add.
	 * @throws QuestionException if the question is malformed.
	 * @throws MoreThan4ChoicesException if the question contains more than 4 answer.
	 * @throws CheckFailException if the question is malformed.
	 */
	public void addQuestion(Question q) throws QuestionException, CheckFailException, MoreThan4ChoicesException
	{
		if (q.check())
			this.getQuestions().add(q);
	}
	
	/**
	 * Reads the deck using the round parameter.
	 * 
	 * @param round		the parameter.
	 * @return the a list of questions with a specified round.
	 */
	public ArrayList<Question> readDeck(Round round)
	{
		ArrayList<Question> paramlist = new ArrayList<Question>();
		for(int i = 0; i < this.getQuestions().size();i++)
		{
			if ((this.getQuestions().get(i).getRound()).equals(round))
				paramlist.add((Question)this.getQuestions().get(i).clone());
		}
		return paramlist;
	}
	
	/**
	 * Replaces a question in the deck by another.
	 * 
	 * @param out	the question to be replaced.
	 * @param in	the question replacing.
	 * @throws QuestionException thrown when there is an error in the deck.
	 * @throws DeckException thrown when there is an error in the deck.
	 * @throws MoreThan4ChoicesException thrown when a question has more than 4 choices.
	 * @throws CheckFailException thrown when a question hasn't a good answer or more than one good answer.
	 */
	public void updateQuestion(Question out, Question in) throws DeckException, QuestionException, CheckFailException, MoreThan4ChoicesException
	{
		int index = this.getQuestions().indexOf(out);
		if (index != -1)
		{	
			if (in.check())
			{
				if (!isDuplicate(in) || (isDuplicate(in) && in.equals(out)))
					this.getQuestions().set(index, in);
			}
		}
		else
			throw new DeckException("Can't find question in deck.");
	}
	
	/**
	 * Deletes the question from the deck.
	 * 
	 * @param q		the question to delete.
	 */
	public void deleteQuestion(Question q)
	{
		this.getQuestions().remove(q);
	}
	
	/**
	 * Checks if a question is already in the deck.
	 * 
	 * @param q		the question tested.
	 * @return		true if the question is a duplicate, else false.
	 */
	public boolean isDuplicate(Question q)
	{
		return this.getQuestions().contains(q);
	}	
	
	/**
	 * Converts the Question object into a JSon String representation.
	 * 
	 * @return		the JSon String representation 	of the question.	
	 */
	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
	
	/**
	 * Deserializes the JSon String representation into a Question object.
	 * 
	 * @param json	the JSon String representation.
	 * @return the Deck.
	 */
	public static Deck fromJson(String json)
	{
		Type listOfQuestions = new TypeToken<Deck>(){}.getType();
		Gson gson = new GsonBuilder().create();
		Deck d = gson.fromJson(json, listOfQuestions);
		return d;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return questions.toString();
	}
	
	/**
	 * Gets the list of questions.
	 * 
	 * @return		the list of questions.
	 */
	public ArrayList<Question> getQuestions() 
	{
		return questions;
	}
	
	/**
	 * Sets the list of questions.
	 * 
	 * @param questions		the list of questions to set.
	 */
	public void setQuestions(ArrayList<Question> questions) 
	{
		this.questions = questions;
	}
	
	
	/**
	 * gets the list of questions for the specified round.
	 * @return the list of questions for the specified round.
	 */
	public ArrayList<Question> getRound1() {
		if(round1==null){
			
			round1 = this.readDeck(Round.FIRST_ROUND);
		}
		return round1;
	}
	/**
	 * gets the list of questions for the specified round.
	 * @return the list of questions for the specified round.
	 */
	public ArrayList<Question> getRound2() {
		if(round2==null){
			round2 = this.readDeck(Round.SECOND_ROUND);
		}
		return round2;
	}
	/**
	 * gets the list of questions for the specified round.
	 * @return the list of questions for the specified round.
	 */
	public ArrayList<Question> getRound3() {
		if(round3==null){
			round3 = this.readDeck(Round.LAST_ROUND);
		}
		return round3;
	}
	
	/**
	 * gets a random question for the specified round.
	 * @param compt the specified round.
	 * @return a random question.
	 */
	public Question getRandomQuestion(int compt){
		Question q=new Question();
		if(compt<5){
			int tmp=new Random().nextInt(getRound1().size());
			q=getRound1().get(tmp);
			getRound1().remove(tmp);
		}
		else if(compt>4 && compt<10){
			int tmp=new Random().nextInt(getRound2().size());
			q=getRound2().get(tmp);
			getRound2().remove(tmp);
		}
		else{
			int tmp=new Random().nextInt(getRound3().size());
			q=getRound3().get(tmp);
			getRound3().remove(tmp);
		}
		return q;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o)
	{
		if (o instanceof Deck)
		{
			Deck d = (Deck) o;
			return this.getQuestions().equals(d.getQuestions());
		}
		return false;
	}
	
}