package model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import util.Password;

/**
 * this class represents a user with his informations.
 * @author Lipani Corrado
 *
 */
public class User 
{
	private String name;
	private String password;
	
	/**
	 * the default constructor of the class.
	 * @param name the name of the user.
	 * @param password the password of the user.
	 */
	public User(String name, String password) 
	{
		this.name = name;
		this.password = Password.getSaltedHash(password, Password.getGeneratedSalt());
	}
	
	/**
	 * Builds a User instance from a Json.
	 * @param json the representation json of the class.
	 * @return an instance of User.
	 */
	public static User fromJson(String json)
	{
		Gson gson = new GsonBuilder().create();
		User u = gson.fromJson(json, User.class);
		return u;
	}
	
	/**
	 * gives a json representation of the class.
	 * @return the json representation of the class.
	 */
	public String toJson()
	{
		String json;
		Gson gson = new GsonBuilder().create();
		json = gson.toJson(this);
		return json;
	}
	
	/**
	 * gets the name of the user.
	 * @return the name of the user.
	 */
	public String getName() 
	{
		return name;
	}

	/**
	 * gets the password of the user.
	 * @return the password of the user.
	 */
	public String getPassword() 
	{
		return password;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		return this.getName()+", "+this.getPassword();
	}

}
