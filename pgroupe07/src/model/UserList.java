package model;

import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import util.Serialisation;

/**
 * this class represents a list of users.
 * @author Lipani Corrado
 *
 */
public class UserList 
{
	private static UserList instance;
	private List<User> userlist;
		
	/**
	 * default constructor for the class.
	 */
	private UserList() 
	{
		userlist = new ArrayList<User>();
	}

	/**
	 * Gets the only instance of the UserList.
	 * 
	 * @return		the only instance of the UserList.
	 */
	public static UserList getInstance()
	{
		if (instance == null){
			instance=UserList.fromJson(Serialisation.readFile("userlist.json"));
			if(instance==null)
				instance=new UserList();
		}
		return instance;
	}
	
	/**
	 * Builds a UserList instance from a Json.
	 * @param json the json representation of the class.
	 * @return an instance of UserList.
	 */
	public static UserList fromJson(String json)
	{
		Type listOfUsers = new TypeToken<UserList>(){}.getType();
		Gson gson = new GsonBuilder().create();
		UserList ul = gson.fromJson(json, listOfUsers);
		return ul;
	}
	
	/**
	 * Adds a user to the UserList.
	 * @param u a new user.
	 */
	public void addUser(User u)
	{
		this.getUserlist().add(u);
	}

	/**
	 * gives a json representation of the class.
	 * @return the json representation of the class.
	 */
	public String toJson()
	{
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}

	/**
	 * gets the list of all user.
	 * @return the list of all user.
	 */
	public List<User> getUserlist() 
	{
		return userlist;
	}

}
