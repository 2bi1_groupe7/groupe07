package model;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import util.Serialisation;

/**
 * This class represents a list of score with informations for each score.
 * 
 * @author Lipani Corrado
 *
 */
public class ScoreList extends Observable
{
	private static ScoreList instance = null;
	private ArrayList<Score> scores;
	
	/**
	 * default constructor
	 */
	private ScoreList()
	{
		scores=new ArrayList<Score>();
	}
	
	/**
	 * Gets the only instance of the ScoreList.
	 * 
	 * @return the only instance of the ScoreList.
	 */
	public static ScoreList getInstance()
	{
		if (instance == null)
		{
			instance =  ScoreList.fromJson(Serialisation.readFile("scores.json"));
			if (instance == null)
			{
				instance = new ScoreList();
			}
			
		}
		return instance;
	}
	
	/**
	 * adds a Score to the ScoreList
	 * @param s the new Score
	 * @return true if the score is added to the ScoreList, else false;
	 */
	public boolean addScore(Score s)
	{
		if (!this.getScores().contains(s))
		{
			this.getScores().add(s);
			Serialisation.writeFile("scores.json", getInstance().toJson());
			notifyObs();
			return true;
		}
		return false;
	}
	
	/**
	 * Deletes a Score from the ScoreList.
	 * @param s the score.
	 * @return true if the score is deleted, else false.
	 */
	public boolean delScore(Score s)
	{
		return this.getScores().remove(s);
	}
	
	/**
	 * gets the list of Scores
	 * @return the list of Score
	 */
	public ArrayList<Score> getScores() 
	{
		return scores;
	}
	/**
	 * gives a json representation of the class
	 * @return the json representation of the class
	 */
	public String toJson()
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
	
	/**
	 * Builds a ListScore instance from a Json
	 * @param json the json representation for the class
	 * @return a class ListScore.
	 */
	public static ScoreList fromJson(String json)
	{
		Type listOfScores = new TypeToken<ScoreList>(){}.getType();
		Gson gson = new GsonBuilder().create();
		ScoreList s = gson.fromJson(json, listOfScores);
		return s;
	}
}
