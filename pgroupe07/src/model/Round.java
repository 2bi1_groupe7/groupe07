package model;

/**
 * that class implements an enumeration of Round. A round represents a difficulty for the question
 * @author Franco Maxime
 *
 */
public enum Round 
{
	FIRST_ROUND ("First Round"),
	SECOND_ROUND ("Second Round"),
	LAST_ROUND ("Last Round");
	
	private Round(String desc)
	{
		this.desc = desc;
	}
	public final String desc;
}
