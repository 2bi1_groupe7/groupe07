package model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 *  This class represents a score with the information from the player and the game.
 * @author Lipani Corrado
 *
 */
public class Score 
{
	private String name;
	private int questions;
	private String datetime, pokemon;
	
	
	/**
	 * default constructor of the class with all information to create a score.
	 * @param name name of the player.
	 * @param questions the question.
	 * @param pokemon the earned pokemon.
	 * @param datetime the date of the game.
	 */
	public Score(String name, int questions, String pokemon, String datetime) 
	{
		this.name = name;
		this.questions = questions;
		this.pokemon = pokemon;
		this.datetime = datetime;
	}
	
	/**
	 * gives a representation json of the class
	 * @return the representation json of the class
	 */
	public String toJson()
	{
		String json;
		Gson gson = new GsonBuilder().create();
		json = gson.toJson(this);
		return json;
	}
	
	/**
	 * Builds a class Score from a Json
	 * @param json the representation json of the class
	 * @return a class Score.
	 */
	public static Score fromJson(String json)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Score s = gson.fromJson(json, Score.class);
		return s;
	}
	
	/**
	 * gets the name of the player.
	 * @return the name of the player.
	 */
	public String getName() 
	{
		return name;
	}

	/**
	 * gets the index of the last good question.
	 * @return  the index of the last good question.
	 */
	public int getQuestions() 
	{
		return questions;
	}


	/**
	 * gets the date of the game.
	 * @return the date of the game.
	 */
	public String getDatetime() 
	{
		return datetime;
	}

	/**
	 * gets the earned Pok�mon during the game.
	 * @return the earned Pok�mon during the game.
	 */
	public String getPokemon() 
	{
		return pokemon;
	}	
}
