package model;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents an observable object.
 * An observable object can have one or more observers.
 * @author Lipani Corrado
 *
 */
public abstract class Observable 
{
	private transient List<Observer> obs = new ArrayList<Observer>();
	
	/**
	 * Adds an observer to the class
	 * @param o an observer
	 * @return true if observer is added. else false.
	 */
	public boolean addObs(Observer o)
	{
		if(obs.contains(o))
			return false;
		obs.add(o);
		return true;
	}
	
	/**
	 * Removes an observer from the class
	 * @param o the observer
	 * @return true if the observer is removed, else false.
	 */
	public boolean delObs(Observer o)
	{
		if(obs.contains(o))
		{
			obs.remove(o);
			return true;
		}
		return false;
	}
	
	/**
	 * Notifies each observer that the class has changed.
	 * 
	 */
	public void notifyObs()
	{
		for(int i=0; i < obs.size();i++)
		{
			obs.get(i).update();
		}
	}
}
