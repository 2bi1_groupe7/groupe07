package model;
/**
 * that class implements an enumeration of Specialities. A speciality represents a person who has a better knowledge in a certain domain.
 * @author Franco Maxime
 *
 */
public enum Specialities {
	MOM("Mom"),
	HISTORIAN("Historian"),
	SCIENTIST ("Scientist"),
	GEEK ("Geek");
	
	private Specialities(String desc)
	{
		this.desc = desc;
	}
	public String toString(){
		return this.desc;
	}
	public final String desc;

}
