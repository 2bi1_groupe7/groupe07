package model;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import util.Serialisation;

/**
 * The class representing a list of Keyword for the game.
 * 
 * @author Franco Maxime
 *
 */
public class Keyword extends Observable{
	private ArrayList<ArrayList<String>> words;
	private static Keyword keyword=null;

	/**
	 * Default constructor.
	 * 
	 */
	private Keyword() {
		words=new ArrayList<ArrayList<String>>();
		prepareClass();		
	}
	
	/**
	 * Gets the only instance of the Keyword.
	 * 
	 * @return the only instance of the Keyword.
	 */
	public static Keyword getInstance()	{
		if (keyword == null)		{
			keyword=Keyword.fromJson(Serialisation.readFile("keyword.json"));
			if(keyword==null)
				keyword=new Keyword();
		}
		return keyword;
	}
	
	/**
	 * updates a keyword.
	 * 
	 * @param word the new keyword.
	 * @param column the speciality.
	 * @param row the old keyword.
	 */
	public void updateKeyword(String word,int column, int row){
		words.get(column).remove(row);
		words.get(column).add(row, word);
	}
	
	/**
	 * adds a new keyword in the list.
	 * @param word a new keyword.
	 * @param s the speciality.
	 * @return true if word is added, else false.
	 */
	public boolean addKeyword(String word,Specialities s){
		if(this.contains(word, s)){
			return false;
		}else{
			words.get(s.ordinal()).add(word);
			return true;
		}
	}
	
	/**
	 * Deletes a keyword from a speciality.
	 * 
	 * @param word the keyword.
	 * @param s the speciality.
	 * @return true if word is deleted, else false.
	 */
	public boolean delKeyword(String word,Specialities s)	{
		return words.get(s.ordinal()).remove(word);
	}
	
	/**
	 * Deletes a keyword from a speciality.
	 * @param column the speciality.
	 * @param row the index of the word.
	 * @return true if word is deleted, else false;
	 */
	public boolean delKeyword(int column, int row){
		if(words.get(column).remove(row)!=null){
			return true;
		}
		return false;
	}
	
	/**
	 * gives a representation json of the class
	 * @return the representation json of the class
	 */
	public String toJson(){
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(this);
	}
	
	/**
	 * Builds a class Keyword from a Json
	 * @param json the representation json of the class
	 * @return a class Keyword.
	 */
	public static Keyword fromJson(String json)	{
		Type listOfKeywords = new TypeToken<Keyword>(){}.getType();
		Gson gson = new GsonBuilder().create();
		Keyword words = gson.fromJson(json, listOfKeywords);
		return words;
	}
	/**
	 * Gets all keyword from the class.
	 * @return an ArrayList of ArrayList of keyword.
	 */
	public ArrayList<ArrayList<String>> getWords() {
		return words;
	}
	
	/**
	 * sets all keyword in the class with the param.
	 * @param words a new list of keyword.
	 */
	public void setWords(ArrayList<ArrayList<String>> words) {
		this.words = words;
	}
	
	/**
	 * prepares an ArrayList of keyword for each speciality.
	 */
	private void prepareClass(){
		if(this.words.size()==0){
			for(int i=0;i<Specialities.values().length;i++){
				this.words.add(new ArrayList<String>());
			}
		}
	}
	
	/**
	 * Returns true if this collection contains the word in the specified speciality.
	 * @param word the keyword.
	 * @param s the speciality.
	 * @return true if the list contains the keyword, else false.
	 */
	public boolean contains(String word, Specialities s){
		return words.get(s.ordinal()).contains(word);
	}
	
	/**
	 * Checks if the speciality have a keyword in the question.
	 * @param q the question
	 * @param s the speciality
	 * @return true if the speciality have a keyword in the question, else false.
	 */
	public boolean checkQuestion( String q, Specialities s){
		for(int i=0; i<words.get(s.ordinal()).size();i++){
			if(q.contains(words.get(s.ordinal()).get(i))){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the size of the biggest list of keyword.
	 * @return the size of the biggest list of keyword.
	 */
	public int getSize(){
		int size=words.get(0).size();
		for(int i=1;i<words.size();i++){
			if(size<words.get(i).size()){
				size=words.get(i).size();
			}
		}
		return size;
	}
	
	/**
	 * gets a keyword from a speciality and an index.
	 * @param row the index of the keyword.
	 * @param column the speciality.
	 * @return a keyword from a speciality and an index.
	 */
	public String get(int row, int column){
		if(words.size()-1<column || words.get(column).size()-1<row){
			return "";
		}
		return words.get(column).get(row);
	}
	
}
