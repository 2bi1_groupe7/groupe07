package GUI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

/**
 * The class representing the Login panel for the game.
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
public class LoginPanel extends JPanel
{
	private JPanel jplogo, jplogin;
	private JButton jbvalidate;
	private JLabel jlname, jlpasswrd;

	private JTextField jtfname;
	private JPasswordField jpf;
	private String name = "";

	/**
	 * default constructor.
	 */
	public LoginPanel()
	{
		GridLayout grid = new GridLayout(1,2);
		this.setLayout(grid);
		this.add(this.getJplogo());
		this.add(this.getJplogin());
		
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g) 
	{
		super.paintComponent(g);
		Image img;
		try 
		{
			img = ImageIO.read(new File("img/backgroundtest2.PNG"));
		     g.drawImage(img, 0, 0, this);

		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
	}

	/**
	 * gets the panel logo
	 * @return the panel logo.
	 */
	public JPanel getJplogo() 
	{
		if (jplogo == null)
		{
			jplogo = new JPanel();
			jplogo.setOpaque(false);
		}
		return jplogo;
	}

	/**
	 * gets the panel login.
	 * @return the panel login.
	 */
	public JPanel getJplogin() 
	{
		if (jplogin == null)
		{
			jplogin = new JPanel(){
				public void paintComponent(Graphics g)
				{
					Color bg = Color.decode("#0E3058");
					Color bg2 = new Color(0,0,0,127);
					g.setColor(bg2);
					g.fillRect(this.getWidth()/9, this.getHeight()/3, this.getWidth()-this.getWidth()/5, this.getHeight()/3);
					g.setColor(Color.GRAY);
					g.drawRect(this.getWidth()/9, this.getHeight()/3, this.getWidth()-this.getWidth()/5, this.getHeight()/3);
					g.setColor(Color.DARK_GRAY);
					g.drawString("Java Project, 2nd year of Bachelor's Degree in IT Management, HELHa Mons.", (this.getWidth()/3)-20,this.getHeight()-10);
					Image img;
					try {
						img = ImageIO.read(new File("img/logo.png"));
						g.drawImage(img, this.getWidth()/9, 0, this);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
			};	
			jplogin.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			
			gbc.insets = new Insets(5, 10, 5, 10);
			gbc.gridx = 0;
			gbc.gridy = 0;
			jplogin.add(this.getJlname(), gbc);
			gbc.gridx = 1;
			gbc.gridy = 0;
			jplogin.add(this.getJtfname(), gbc);
			gbc.gridx = 0;
			gbc.gridy = 1;
			jplogin.add(this.getJlpasswrd(), gbc);
			gbc.gridx = 1;
			gbc.gridy = 1;
			jplogin.add(this.getJpf(), gbc);
			gbc.gridx = 1;
			gbc.gridy = 3;
			gbc.anchor=GridBagConstraints.EAST;
			jplogin.add(this.getJb(), gbc);	
		}
		return jplogin;
	}
	
	/**
	 * gets the button play.
	 * @return the button play.
	 */
	public JButton getJb() 
	{
		if (jbvalidate == null)
		{
			jbvalidate = new JButton("Play");
			jbvalidate.setPreferredSize(new Dimension(100, 30));
			jbvalidate.setBackground(Color.LIGHT_GRAY);
			jbvalidate.setBorder(BorderFactory.createLineBorder(Color.decode("#000000")));
			jbvalidate.setFocusable(false);
		}
		return jbvalidate;
	}
	
	/**
	 * gets the JLabel name.
	 * @return the JLabel name.
	 */
	public JLabel getJlname() 
	{
		if (jlname == null)
		{
			jlname = new JLabel("Username : ");
			jlname.setForeground(Color.white);
		}
		return jlname;
	}
	
	/**
	 * gets the JTextField name.
	 * @return the JTextField name.
	 */
	public JTextField getJtfname() 
	{
		if (jtfname == null)
		{
			jtfname = new JTextField(15);
			jtfname.setBackground(Color.decode("#ffffff"));
			Border line = BorderFactory.createLineBorder(Color.decode("#000000"));
			Border empty = new EmptyBorder(0, 5, 0, 0);
			CompoundBorder cb = new CompoundBorder(line, empty);
			jtfname.setBorder(cb);
		}
		return jtfname;
	}
	
	/* (non-Javadoc)
	 * @see java.awt.Component#getName()
	 */
	public String getName() 
	{
		return name;
	}

	/* (non-Javadoc)
	 * @see java.awt.Component#setName(java.lang.String)
	 */
	public void setName(String name) 
	{
		this.name = name;
	}

	/**
	 * gets the JLabel password.
	 * @return the JLabel password.
	 */
	public JLabel getJlpasswrd() 
	{
		if (jlpasswrd == null)
		{
			jlpasswrd = new JLabel("Password :");
			jlpasswrd.setForeground(Color.white);
		}
		return jlpasswrd;
	}

	/**
	 * gets the JPasswordField password.
	 * @return the JPasswordField password.
	 */
	public JPasswordField getJpf()
	{
		if (jpf == null)
		{
			jpf = new JPasswordField(15);
			jpf.setBackground(Color.decode("#ffffff"));
			jpf.setBorder(BorderFactory.createLineBorder(Color.decode("#000000")));
			Border line = BorderFactory.createLineBorder(Color.decode("#000000"));
			Border empty = new EmptyBorder(0, 5, 0, 0);
			CompoundBorder cb = new CompoundBorder(line, empty);
			jpf.setBorder(cb);
		}
		return jpf;
	}
}

