package GUI;



import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;
import java.awt.CardLayout;
import javax.imageio.ImageIO;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import model.Deck;
import model.Keyword;
import model.Question;
import model.Score;
import model.ScoreList;
import model.Specialities;
import util.DateTime;


/**
 * This class represents the panel game.
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
public class GamePanel extends JPanel
{
	private JLabel jlQuestion, jlJoker, jlGain, jlReturn, joker1, joker2, joker3;
	private JPanel jpCenter;
	private Question q;
	private Deck d;
	private Keyword k;
	private int compt;
	private int[] repSup=new int[2];
	private ArrayList<JLabel> labels;
	private ArrayList<JLabel> choix;
	private String pokemon[] = {"Abra", "Caterpie", "Rattata", "Pidgey", "Pikachu","Ivysaur", "Wartortle","Charmeleon","Snorlax", "Alakazam", "Gengar",
			"Dragonite", "Gyarados", "Charizard", "Mewtwo"};
	private boolean isJoker[]={false,false,false};
	
	
	/**
	 * default constructor.
	 */
	public GamePanel()
	{
		this.setLayout(new GridBagLayout());
		this.add(getJpCenter());
		Color bg = Color.decode("#19549A");
		setBackground(bg);
		Locale locale = Locale.ENGLISH;
		JOptionPane.setDefaultLocale(locale);
		
		gameLaunch();
		nextInterface();
		
	}

	/**
	 * gets the labels.
	 * @return the labels.
	 */
	public ArrayList<JLabel> getLabels() {
		if(labels==null)
		{
			labels= new ArrayList<JLabel>();
			for(int i=0;i<15;i++)
			{
				JLabel tmp= new JLabel(new ImageIcon("sprites/"+pokemon[i].toLowerCase()+".png"));
				tmp.setToolTipText("Question n�"+(i+1));
				labels.add(tmp);
			}
			
			
		}
		return labels;
	}
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	protected void paintComponent(Graphics g)
    {
      
        super.paintComponent(g);
        Image img;
		try 
		{
			img = ImageIO.read(new File("img/backgroundgame.png"));
		     g.drawImage(img, 0, 0, this);

		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
    }

	/**
	 * gets the panel center.
	 * @return the panel center.
	 */
	public JPanel getJpCenter() {
		if(jpCenter == null){
			jpCenter = new JPanel();
			GroupLayout gp2 = new GroupLayout(jpCenter);
            jpCenter.setLayout(gp2);
			gp2.setAutoCreateGaps(true);
			gp2.setAutoCreateContainerGaps(true);
			jpCenter.setOpaque(false);
			gp2.setHorizontalGroup(gp2.createSequentialGroup()
					.addGroup(gp2.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addGroup(gp2.createSequentialGroup()
							.addComponent(getLabels().get(0), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(1), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(2), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(3), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(4), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(5), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(6), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(7), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(8), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(9), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(10), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(11), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(12), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(13), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							.addComponent(getLabels().get(14), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
							          GroupLayout.PREFERRED_SIZE)
							)
					.addGroup(gp2.createSequentialGroup()
							.addGroup(gp2.createParallelGroup(GroupLayout.Alignment.CENTER)
								.addComponent(getJlGain(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(getJlquestion())
								.addComponent(getJlJoker())
								.addGroup(gp2.createSequentialGroup()
										.addComponent(getJoker1())
										.addGap(50)
										.addComponent(getJoker2())
										.addGap(50)
										.addComponent(getJoker3())
								)
								.addGroup(gp2.createSequentialGroup()
											.addComponent(getChoix().get(0))
											.addComponent(getChoix().get(1))
										
								)
										
								.addGroup(gp2.createSequentialGroup()
											.addComponent(getChoix().get(2))
											.addComponent(getChoix().get(3))
										
								)
							)
							.addComponent(getJlReturn())
					)
			));
			gp2.setVerticalGroup(gp2.createSequentialGroup()
					
					.addGroup(gp2.createParallelGroup(GroupLayout.Alignment.BASELINE)
							.addComponent(getLabels().get(0),80,80,80)
							.addComponent(getLabels().get(1),80,80,80)
							.addComponent(getLabels().get(2),80,80,80)
							.addComponent(getLabels().get(3),80,80,80)
							.addComponent(getLabels().get(4),80,80,80)
							.addComponent(getLabels().get(5),80,80,80)
							.addComponent(getLabels().get(6),80,80,80)
							.addComponent(getLabels().get(7),80,80,80)
							.addComponent(getLabels().get(8),80,80,80)
							.addComponent(getLabels().get(9),80,80,80)
							.addComponent(getLabels().get(10),80,80,80)
							.addComponent(getLabels().get(11),80,80,80)
							.addComponent(getLabels().get(12),80,80,80)
							.addComponent(getLabels().get(13),80,80,80)
							.addComponent(getLabels().get(14),80,80,80)
							
							)
							.addComponent(getJlGain(),80,80,80)
							.addComponent(getJlquestion(),140,220,350)
					.addComponent(getJlJoker(),125,220,350)
					.addGroup(gp2.createParallelGroup(GroupLayout.Alignment.BASELINE)
							.addComponent(getJoker1(),90,90,90)
							.addComponent(getJoker2(),90,90,90)
							.addComponent(getJoker3(),90,90,90)
								)
					.addGroup(gp2.createParallelGroup(GroupLayout.Alignment.BASELINE)
							.addGroup(gp2.createSequentialGroup()
								.addGroup(gp2.createParallelGroup(GroupLayout.Alignment.BASELINE)
												.addComponent(getChoix().get(0))
												.addComponent(getChoix().get(1))
											
								)
								.addGroup(gp2.createParallelGroup(GroupLayout.Alignment.BASELINE)
												.addComponent(getChoix().get(2))
												.addComponent(getChoix().get(3))
								)
							)
							.addComponent(getJlReturn())
						)
					);

		}
		return jpCenter;
	}
	
	/**
	 * gets the labels gain.
	 * @return the labels gain.
	 */
	public JLabel getJlGain() {
        if (jlGain == null)
        {
            ImageIcon img = new ImageIcon("img/quit.png");
            jlGain = new JLabel("",img,SwingConstants.CENTER);
            jlGain.setForeground(Color.WHITE);
            jlGain.setHorizontalTextPosition(JLabel.CENTER);
            jlGain.addMouseListener(new MouseListener() {

                @Override
                public void mouseReleased(MouseEvent e) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void mousePressed(MouseEvent e) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void mouseExited(MouseEvent e) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    if(compt!=1){
                        ImageIcon ic=new ImageIcon("sprites/"+pokemon[compt-2].toLowerCase()+".PNG");
                        int dialogResult = JOptionPane.showConfirmDialog (GamePanel.this,"Well done, do you want to stop and leave with "+pokemon[compt-2]+" ?","Congratulations",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,ic);
                        if(dialogResult == JOptionPane.YES_OPTION){
                            saveScore(pokemon[compt-2]);
                            Container cont = ((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).getContentPane();
                            ((CardLayout)cont.getLayout()).show(cont, "SCORE");
                            ((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).getLogpanel().getJtfname().setText("");
                            ((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).getLogpanel().getJpf().setText("");
                            resetGame();
                        }
                    }
                }
            });
        }
        return jlGain; 
    }
	
	/**
	 * gets the joker 1.
	 * @return the joker 1.
	 */
	public JLabel getJoker1() 
	{
		if(joker1 == null)
		{
			ImageIcon ico1 = new ImageIcon("img/pokematos.png");
			joker1 = new JLabel(ico1);
			joker1.setToolTipText("Ask your friend for help");
			joker1.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mousePressed(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseExited(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent arg0) {
					int count=0;
					getJlJoker().setVisible(true);
					getJlJoker().setText("Choose the person you want to talk to");
					setJoker(true,0);
					for(int i=0; i<getChoix().size();i++){
						if(!getChoix().get(i).isVisible()){
							repSup[count]=i;
							getChoix().get(i).setVisible(true);
							count++;
						}
					}
					for(int i=0;i<getChoix().size();i++){
						getChoix().get(i).setText(Specialities.values()[i].toString());
					}
					getJoker1().setVisible(false);	
				}
			});
		}
		return joker1;
	}

	/**
	 * gets the joker 2.
	 * @return the joker 2.
	 */
	public JLabel getJoker2() 
	{
		if(joker2 == null)
		{
			ImageIcon ico2 = new ImageIcon("img/audience.png");
			joker2 = new JLabel(ico2);
			joker2.setToolTipText("Let's your starters tell you what they believe is the right answer");
			joker2.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					if(!isJoker[0]){
						Random rand=new Random();
						int ratio,rightAnswer,wrongAnswer[]={0,0,0},count=0;
						String tmp="<html>Here are the results coming from the audience.<br>";
						switch(q.getRound()){
						case FIRST_ROUND:
							ratio=3;
							break;
						case SECOND_ROUND:
							ratio=2;
							break;
						case LAST_ROUND:
							ratio=1;
							break;
						default:
							ratio=0;
							System.out.println("Error");
							break;
						}
						if(!isJoker[2]){
							rightAnswer=ratio*(16-compt)+25+rand.nextInt(10);
							wrongAnswer[0]=rand.nextInt((100-rightAnswer)/2);
							wrongAnswer[1]=rand.nextInt(100-rightAnswer-wrongAnswer[0]-rand.nextInt(11));
							wrongAnswer[2]=100-rightAnswer-wrongAnswer[0]-wrongAnswer[1];
							char c ='A';
							for(int i=0;i<getChoix().size();i++){
								if(!q.checkAnswer( getChoix().get(i).getText())){
									tmp+="Answer "+c+" : "+wrongAnswer[count]+"%&nbsp;&nbsp;";
									count++;
								}
								else{
									tmp+="Answer "+c+" : "+rightAnswer+"%&nbsp;&nbsp;";
								}
								c++;
							}
						}
						else{
							rightAnswer=ratio*16+25+rand.nextInt(10);
							wrongAnswer[0]=100-rightAnswer;
							char c ='A';
							for(int i=0;i<getChoix().size();i++){
								if(!q.checkAnswer( getChoix().get(i).getText())&&getChoix().get(i).isVisible()){
									tmp+="Answer "+c+" : "+wrongAnswer[count]+"%&nbsp;&nbsp;";
									count++;
								}
								else if(getChoix().get(i).isVisible()){
									tmp+="Answer "+c+" : "+rightAnswer+"%&nbsp;&nbsp;";
								}
								c++;
							}
						}
						getJlJoker().setVisible(true);
						getJlJoker().setText(tmp);
						getJoker2().setVisible(false);
					}
				}
			});
		}
		return joker2;
	}
	
	/**
	 * gets the joker 3.
	 * @return the joker 3.
	 */
	public JLabel getJoker3() 
	{
		if(joker3 == null)
		{
			ImageIcon ico3 = new ImageIcon("img/oak.png");
			joker3 = new JLabel(ico3);
			joker3.setToolTipText("Eliminates two wrong answers");
			joker3.setPreferredSize(new Dimension(80,30));
			joker3.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
		
				@Override
				public void mouseClicked(MouseEvent e) {
					if(!isJoker[0]){
						int badAnswer[]={q.badAnswer(),q.badAnswer()};
						setJoker(true, 2);
						while(badAnswer[0]==badAnswer[1]){
							badAnswer[1]=q.badAnswer();
						}
						for(int i=0;i<badAnswer.length;i++){
							getChoix().get(badAnswer[i]).setVisible(false);
						}
						joker3.setVisible(false);
					}
				}
			});
		}
		return joker3;
	}
	
	/**
	 * gets the ArrayList of choices.
	 * @return the ArrayList of choices.
	 */
	public ArrayList<JLabel> getChoix() {
		if (choix == null)
		{
			choix = new ArrayList<JLabel>();
			JLabel tmp;
			char letter='A';
			ImageIcon img = new ImageIcon("img/answerbutton.png");
			for(int i=0; i<4;i++){
				tmp = new JLabel("",img,SwingConstants.CENTER);
				tmp.setToolTipText("Answer "+letter++);
				tmp.addMouseListener(new MouseListener() {
					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub	
					}
					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub	
					}
					
					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub	
					}
					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub	
					}			
					@Override
					public void mouseClicked(MouseEvent e) {
						int index=getChoix().indexOf(e.getSource());
						if(isJoker()[0]){
							if(isJoker()[2]){
								getChoix().get(repSup[0]).setVisible(false);
								getChoix().get(repSup[1]).setVisible(false);
							}
							printAnswer();
							boolean capable= k.checkQuestion(q.getStatement(),Specialities.values()[index]);
							callAFriend(capable);
						}
						else{
							checkAnswer(getChoix().get(index).getText());	
						}
					}
				});
				choix.add(tmp);
			}
		}
		return choix;
	}
	
	/**
	 * gets the label statement of the question.
	 * @return the label statement of the question.
	 */
	public JLabel getJlquestion() 
	{
		if (jlQuestion == null)
		{
			ImageIcon img = new ImageIcon("img/questionlabel.png");
			jlQuestion = new JLabel("",img,SwingConstants.CENTER);
			jlQuestion.setPreferredSize(new Dimension(700, 70));
			jlQuestion.setHorizontalTextPosition(JLabel.CENTER);
		}
		return jlQuestion;
	}
	
	/**
	 * sets all informations for a new game.
	 */
	public void gameLaunch(){
		d = Deck.getInstance();
		k= Keyword.getInstance();
		compt=1;
		for(int i=1;i<15;i++){
			getLabels().get(i).setBackground(null);
		}
		for(int i =0; i < 15; i++)
		{
			getLabels().get(i).setBackground(new Color(0,0,0,120));
			getLabels().get(i).setOpaque(true);
		}
		labels.get(0).setBackground(new Color(255,0,0,120));
		labels.get(4).setBackground(new Color(250,226,11,120));
		labels.get(9).setBackground(new Color(250,226,11,120));
		labels.get(14).setBackground(new Color(250,226,11,120));
		for(int i=0;i<getChoix().size();i++){
			getChoix().get(i).setVisible(true);
		}
		getJoker3().setVisible(true);
		getJoker2().setVisible(true);
		getJoker1().setVisible(true);
		repaint();
	}
	
	/**
	 *  sets all informations for the next question.
	 */
	public void nextInterface(){
		if(getCompt()==1){
			getJlGain().setText("you still have not won anything to leave!");
		}
		else{
			getJlGain().setText("Click here to exit the game with your "+pokemon[compt-2]);
		}
		q=d.getRandomQuestion(this.getCompt());
		getJlJoker().setVisible(false);
		getJlquestion().setText(q.getStatement());
		getJlquestion().setHorizontalTextPosition(JLabel.CENTER);
		getJlquestion().setFont(new Font("TimesRoman", Font.PLAIN,15));
		printAnswer();
		for(int i=0;i<isJoker().length;i++){
			setJoker(false,i);
		}
		for(int i=0;i<getChoix().size();i++){
			getChoix().get(i).setVisible(true);
		}
	}
	
	/**
	 * saves the score of the current game.
	 * @param pokemon the earned Pok�mon.
	 */
	public void saveScore(String pokemon)
	{
		Score score = new Score(" user",compt, pokemon,DateTime.getCurrentDateTime());
		ScoreList scores = ScoreList.getInstance();
		scores.addScore(score);
	}
	
	/**
	 * changes the color of the label.
	 */
	public void changeColor(){
		for(int i=0;i<=compt-1;i++){
			getLabels().get(i).setBackground(new Color(0,153,0,120));
			repaint();
		}
		getLabels().get(compt).setBackground(new Color(255,0,0,120));
		compt++;
	}
	
	/**
	 * gets the label joker.
	 * @return the label joker.
	 */
	public JLabel getJlJoker() {
		if (jlJoker == null)
		{
			ImageIcon img = new ImageIcon("img/audiencebg.png");
			jlJoker = new JLabel("",img,SwingConstants.CENTER);
			jlJoker.setPreferredSize(new Dimension(700, 70));
			jlJoker.setHorizontalTextPosition(JLabel.CENTER);
			jlJoker.setFont(new Font("TimesRoman", Font.PLAIN,15));
		}
		return jlJoker;
	}

	/**
	 * Displays all choices for the question.
	 */
	public void printAnswer(){
		for(int i=0; i<getChoix().size();i++){
			getChoix().get(i).setText((String)q.getChoices().keySet().toArray()[i]);
			getChoix().get(i).setHorizontalTextPosition(JLabel.CENTER);
			getChoix().get(i).setFont(new Font("TimesRoman", Font.PLAIN, 12));
		}
	}

	/**
	 * returns a table of boolean joker.
	 * @return true if the player is using a joker, else false;
	 */
	public boolean[] isJoker() {
		return isJoker;
	}

	/**
	 * sets a boolean true if the player is using a joker.
	 * @param isJoker true if the player is using a joker.
	 * @param i the type of joker.
	 */
	public void setJoker(boolean isJoker, int i) {
		this.isJoker[i] = isJoker;
	}
	
	/**
	 * sets the label joker with different values according to the speciality capable
	 * @param capable if the friend have a speciality.
	 */
	public void callAFriend(boolean capable){
		setJoker(false,0);
		Random rand=new Random();
		int borne=101;
		String rep="";
		if(capable){
			rep+="I'm sure it's : ";
			switch(q.getRound()){
			case FIRST_ROUND:borne=100;break;
			case SECOND_ROUND:borne=90;break;
			case LAST_ROUND:borne=80;break;
			}
		}
		else{
			rep+=" I think it's : ";
			switch(q.getRound()){
			case FIRST_ROUND:borne=70;break;
			case SECOND_ROUND:borne=50;break;
			case LAST_ROUND:borne=30;break;
			}
		}
		if((1+rand.nextInt(100))<borne){
			getJlJoker().setText(rep+q.goodQuestion());
		}
		else{
			if(!isJoker()[2]){
				getJlJoker().setText(rep+getChoix().get(q.badAnswer()).getText());
			}
			else{
				for(int i=0;i<getChoix().size();i++){
					if(!q.checkAnswer(getChoix().get(i).getText())&&getChoix().get(i).isVisible()){
						getJlJoker().setText(rep+getChoix().get(i).getText());
					}					
				}
			}
		}
	}
	
	/**
	 * checks if the player answered correctly.
	 * @param s the answer.
	 */
	public void checkAnswer(String s){
		String pokemon;
		if( compt==1){
			pokemon="Magikarp";
		}
		else if(compt<6){
			pokemon = this.pokemon[0];
		}
		else if(compt<11){
			pokemon = this.pokemon[4];
		}
		else{
			pokemon = this.pokemon[9];
		}
		if(q.checkAnswer(s)){
			if(compt==15){
				saveScore("Mewtwo");
				ImageIcon ic=new ImageIcon("sprites/mewtwo.PNG");
				int dialogResult = JOptionPane.showConfirmDialog (GamePanel.this,"Well done, you won a Mewtwo. Do you want to see the scores ? ","Congratulations",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,ic);
				if(dialogResult == JOptionPane.YES_OPTION){
					Container cont = ((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).getContentPane();
					((CardLayout)cont.getLayout()).show(cont, "SCORE");
				}
				else{
					Container cont = ((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).getContentPane();
					((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).setConnected(false);
					((CardLayout)cont.getLayout()).show(cont, "LOGIN");;
				}
				resetGame();
			}
			else{
				changeColor();
				nextInterface();			
			}
		}
		else{
			saveScore(pokemon);
			ImageIcon ic=new ImageIcon("sprites/"+pokemon.toLowerCase()+".PNG");
			int dialogResult = JOptionPane.showConfirmDialog (GamePanel.this,"Wrong answer, it was "+q.goodQuestion()+", you leave with "+pokemon+". Do you want to retry ?","Game over",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,ic);
			if(dialogResult == JOptionPane.YES_OPTION){
				resetGame();				
			}
			else{
				Container cont = ((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).getContentPane();
				((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).setConnected(false);
				((CardLayout)cont.getLayout()).show(cont, "SCORE");
				((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).getLogpanel().getJtfname().setText("");
				((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).getLogpanel().getJpf().setText("");
				resetGame();			
			}
		}
	}
	
	/**
	 * gets the counter of questions.
	 * @return the counter of questions.
	 */
	public int getCompt() {
		return compt;
	}
	
	/**
	 * resets the game.
	 */
	public void resetGame(){
		gameLaunch();
		nextInterface();	
	}
	
	/**
	 * gets the label return.
	 * @return the label return.
	 */
	public JLabel getJlReturn() {
		if (jlReturn==null){
			ImageIcon img = new ImageIcon("img/backbutton.png");
			jlReturn = new JLabel("",img,SwingConstants.RIGHT);
			jlReturn.addMouseListener(new MouseListener() {
				
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void mouseClicked(MouseEvent e) {
					Container cont = ((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).getContentPane();
					((Frame)SwingUtilities.getWindowAncestor(GamePanel.this)).setConnected(false);
					((CardLayout)cont.getLayout()).show(cont, "LOGIN");
					resetGame();
				}
			});
		}
		return jlReturn;
	}
}