package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import exceptions.CheckFailException;
import exceptions.MoreThan4ChoicesException;
import exceptions.QuestionException;
import model.Deck;
import model.Keyword;
import model.Observer;
import model.Question;
import model.Round;
import model.Specialities;
import util.Serialisation;

/**
 * the panel deck administration
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
public class DeckPanel extends JPanel implements Observer
{
	private JTable jtable;
	private JButton delete, edit, add, back;
	private TableModelDeck tm;
	
	/**
	 * default constructor
	 */
	public DeckPanel()
	{
		this.setLayout(new BorderLayout());
		JScrollPane spTable = new JScrollPane(getJtable());
		this.add(spTable, BorderLayout.CENTER);
		JPanel jp = new JPanel();
		jp.setLayout(new FlowLayout());
		jp.add(this.getAdd());
		jp.add(this.getEdit());
		jp.add(this.getDelete());
		jp.add(this.getBack());
		this.add(jp, BorderLayout.NORTH);
		jp.setBackground(Color.GRAY);
		Deck.getInstance().addObs(this);
	}
	
	/**
	 * gets the table of the deck.
	 * @return the table of the deck. 
	 */
	public JTable getJtable() 
	{
		if (jtable == null)
		{
			jtable = new JTable(this.getTableModel());
			jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			jtable.getTableHeader().setReorderingAllowed(false);
			TableColumn author = jtable.getColumn("Author");
			TableColumn rounds = jtable.getColumn("Round");
			author.setMaxWidth(200);
			author.setMinWidth(200);
			rounds.setMaxWidth(200);
			rounds.setMinWidth(200);
		}
		return jtable;
	}
	
	/**
	 * gets the TableModel.
	 * @return the TableModel.
	 */
	public TableModelDeck getTableModel()
	{
		if (tm == null)
		{
			tm = new TableModelDeck();
		}
		return tm;
	}
	
	/**
	 * this method is called when someone edits a question in the table deck.
	 */
	public void onEdit()
	{
		int row = this.getJtable().getSelectedRow();
		if (row != -1) {
			JFrame addFrame = new JFrame();
			addFrame.setSize(900, 300);
			addFrame.add(new QuestionPanel(tm.getDeck().getQuestions().get(row),row));
			addFrame.setVisible(true);
			addFrame.setTitle("Question Preview");
			addFrame.setLocationRelativeTo(null);
		}
	}
	
	/**
	 * this method is called when someone deletes a question in the table deck.
	 */
	public void onDelete()
	{
		int row = this.getJtable().getSelectedRow();
		if (row != -1)
			this.getTableModel().removeRow(row);
	}
	
	/**
	 * this method is called when someone adds a question in the table deck.
	 */
	public void onAdd()
	{
		JFrame addFrame = new JFrame();
		addFrame.setSize(900, 300);
		addFrame.add(new QuestionPanel());
		addFrame.setVisible(true);
		addFrame.setTitle("Question Preview");
		addFrame.setLocationRelativeTo(null);
	}
	
	/**
	 * gets the button delete.
	 * @return the button delete.
	 */
	public JButton getDelete() 
	{
		if (delete == null)
		{
			delete = new JButton("Delete");
			delete.setPreferredSize(new Dimension(100,30));
			delete.addActionListener(e->onDelete());
		}
		return delete;
	}

	/**
	 * gets the button Edit.
	 * @return the button edit.
	 */
	public JButton getEdit() 
	{
		if (edit == null)
		{
			edit = new JButton("Edit");
			edit.setPreferredSize(new Dimension(100,30));
			edit.addActionListener(e->onEdit());
		}
		return edit;
	}
	
	/**
	 * gets the button Add.
	 * @return the button add.
	 */
	public JButton getAdd() 
	{
		if (add == null)
		{
			add = new JButton("Add");
			add.setPreferredSize(new Dimension(100,30));
			add.addActionListener(e->onAdd());
		}
		return add;
	}
	
	/**
	 * gets the button Back
	 * @return the button back
	 */
	public JButton getBack() 
	{
		if (back == null)
		{
			back = new JButton("Back");
			back.setPreferredSize(new Dimension(100,30));
			back.setBackground(Color.DARK_GRAY);
			back.setForeground(Color.WHITE);
			back.addActionListener(e->{
				Container cont = ((Frame)SwingUtilities.getWindowAncestor(DeckPanel.this)).getContentPane();
				((CardLayout)cont.getLayout()).show(cont, "LOGIN");
			});
		}
		return back;
	}
	
	/* (non-Javadoc)
	 * @see model.Observer#update()
	 */
	public void update() 
	{
		this.getTableModel().fireTableDataChanged();
	}
}


/**
 * this class gives the representation for the table deck.
 * 
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
class TableModelDeck extends AbstractTableModel
{
	private Deck d = Deck.getInstance();
	private final String[] entetes = { "Author", "Statement", "Answers", "Round"};
	
	/**
	 * default constructor.
	 */
	public TableModelDeck()
	{
		super();
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() 
	{
		return d.getQuestions().size();
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int column)
	{
		return entetes[column];
	}
	
	/**
	 * gets the deck .
	 * @return the deck.
	 */
	public Deck getDeck()
	{
		return d;
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() 
	{
		return entetes.length;
	}
	
	/**
	 * removes a question from the deck.
	 * @param row the index of the question.
	 */
	public void removeRow(int row)
    {
		d.getQuestions().remove(row);
		Serialisation.writeFile("Deck2.Json", d.toJson());
        fireTableRowsDeleted(row, row);
    }

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) 
	{
		switch(columnIndex)
		{
			case 0:
				return d.getQuestions().get(rowIndex).getAuthor();	
			case 1:
				return d.getQuestions().get(rowIndex).getStatement();
			case 2:
				return d.getQuestions().get(rowIndex).getChoices();
			case 3:
				return d.getQuestions().get(rowIndex).getRound();
			default:
				return null;
		}
	}
}

/**
 * this class gives a new panel when adding a new question.
 * 
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
class QuestionPanel extends JPanel
{
	private JLabel jlauthor, jlquestion, jlround, jla, jlb, jlc, jld, jlchange, jlKeyword,jlSpeciality;
	private JTextField jtfauth, jtfq, jtfa, jtfb, jtfc, jtfd,jtfKeyword;
	private JComboBox<String> jcbRound,jcbSpeciality;
	private JButton save, cancel, close;
	private JCheckBox jcba, jcbb, jcbc, jcbd;
	private ButtonGroup bg;
	private int editedRow;
	boolean editmode = false;
	
	/**
	 * constructor when someone edits a question
	 * @param q the question
	 * @param row the index of the question.
	 */
	public QuestionPanel(Question q, int row)
	{
		this.editedRow = row;
		initUI();
		setQuestion(q);
		editmode = true;
	}
	
	/**
	 * default constructor
	 */
	public QuestionPanel()
	{
		initUI();
	}
	
	/**
	 * Initializes the panel.
	 */
	public void initUI()
	{
		this.setLayout(new GridBagLayout());
		JPanel center = new JPanel();
		this.add(center);
		GroupLayout gl = new GroupLayout(center);
		center.setLayout(gl);
		
		gl.setAutoCreateGaps(true);
		gl.setAutoCreateContainerGaps(true);
		
		gl.setHorizontalGroup(gl.createSequentialGroup()
				.addGroup(gl.createParallelGroup()
						.addComponent(this.getJlauthor())
						.addComponent(this.getJlquestion())
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(this.getJtfauth(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						          GroupLayout.PREFERRED_SIZE)
						.addComponent(this.getJtfq(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						          GroupLayout.PREFERRED_SIZE)
						.addGroup(gl.createSequentialGroup()
								.addGroup(gl.createParallelGroup()
										.addGroup(gl.createSequentialGroup()
												.addComponent(this.getJla())
												.addComponent(this.getJcba())
												.addComponent(this.getJtfa(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
										.addGroup(gl.createSequentialGroup()
												.addComponent(this.getJlc())
												.addComponent(this.getJcbc())
												.addComponent(this.getJtfc(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
										)
								.addGroup(gl.createParallelGroup()
										.addGroup(gl.createSequentialGroup()
												.addComponent(this.getJlb())
												.addComponent(this.getJcbb())
												.addComponent(this.getJtfb(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
										.addGroup(gl.createSequentialGroup()
												.addComponent(this.getJld())
												.addComponent(this.getJcbd())
												.addComponent(this.getJtfd(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))

										)
								)
						.addGroup(gl.createSequentialGroup()
								.addComponent(this.getJlKeyword(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								          GroupLayout.PREFERRED_SIZE)
								.addComponent(this.getJtfKeyword(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								          GroupLayout.PREFERRED_SIZE)
								.addComponent(this.getJlSpeciality(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								          GroupLayout.PREFERRED_SIZE)
								.addComponent(this.getJcbSpeciality(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								          GroupLayout.PREFERRED_SIZE)
								)
						
						)
				.addComponent(this.getJlround())
				.addGroup(gl.createParallelGroup()
						.addComponent(this.getJcbRound(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						          GroupLayout.PREFERRED_SIZE)
						.addComponent(this.getSave(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						          GroupLayout.PREFERRED_SIZE)
						.addComponent(this.getCancel(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						          GroupLayout.PREFERRED_SIZE)
						.addComponent(this.getClose(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
				          GroupLayout.PREFERRED_SIZE)
						)
				);
		gl.setVerticalGroup(gl.createParallelGroup()
				.addGroup(gl.createSequentialGroup()
						.addComponent(this.getJlauthor())
						.addComponent(this.getJlquestion())
				)
				.addGroup(gl.createSequentialGroup()
						.addComponent(this.getJtfauth(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						          GroupLayout.PREFERRED_SIZE)
						.addComponent(this.getJtfq(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						          GroupLayout.PREFERRED_SIZE)
						.addGroup(gl.createParallelGroup()
								.addGroup(gl.createSequentialGroup()
										.addGroup(gl.createParallelGroup()
												.addComponent(this.getJla())
												.addComponent(this.getJcba())
												.addComponent(this.getJtfa(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
										.addGroup(gl.createParallelGroup()
												.addComponent(this.getJlc())
												.addComponent(this.getJcbc())
												.addComponent(this.getJtfc(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
	

										)
								.addGroup(gl.createSequentialGroup()
										.addGroup(gl.createParallelGroup()
												.addComponent(this.getJlb())
												.addComponent(this.getJcbb())
												.addComponent(this.getJtfb(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
										.addGroup(gl.createParallelGroup()
												.addComponent(this.getJld())
												.addComponent(this.getJcbd())
												.addComponent(this.getJtfd(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
														GroupLayout.PREFERRED_SIZE))
										
										)
								)
						.addGroup(gl.createParallelGroup()
								.addComponent(this.getJlKeyword(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								          GroupLayout.PREFERRED_SIZE)
								.addComponent(this.getJtfKeyword(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								          GroupLayout.PREFERRED_SIZE)
								.addComponent(this.getJlSpeciality(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								          GroupLayout.PREFERRED_SIZE)
								.addComponent(this.getJcbSpeciality(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								          GroupLayout.PREFERRED_SIZE)
								)
						)
				.addComponent(this.getJlround())
				.addGroup(gl.createSequentialGroup()
				.addComponent(this.getJcbRound(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
				          GroupLayout.PREFERRED_SIZE)
				.addComponent(this.getSave(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
				          GroupLayout.PREFERRED_SIZE)
				.addComponent(this.getCancel(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
				          GroupLayout.PREFERRED_SIZE)
				.addComponent(this.getClose(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
				          GroupLayout.PREFERRED_SIZE)
				));
	}
	
	/**
	 * Sets all the information of a question in the panel's component.
	 * @param q the question
	 */
	public void setQuestion(Question q)
	{
		this.getJtfauth().setText(q.getAuthor());
		this.getJcbRound().setSelectedItem(q.getRound().name());
		this.getJtfq().setText(q.getStatement());
		this.getBg().clearSelection();
		HashMap<String, Boolean> hm = q.getChoices();
		List<JTextField> Jtflist = new ArrayList<JTextField>();
		Jtflist.add(getJtfa());
		Jtflist.add(getJtfb());
		Jtflist.add(getJtfc());
		Jtflist.add(getJtfd());
		List<JCheckBox> Jcblist = new ArrayList<JCheckBox>();
		Jcblist.add(getJcba());
		Jcblist.add(getJcbb());
		Jcblist.add(getJcbc());
		Jcblist.add(getJcbd());
		int n = 0;
		for(String key : hm.keySet())
		{
			if (hm.get(key))
				Jcblist.get(n).setSelected(true);
			Jtflist.get(n).setText(key);
			n++;
		}
	}
	
	/**
	 * gets all the information from a question in the panel's component.
	 * @return a new question
	 */
	public Question getUpdatedQuestion()
	{
		String author = getJtfauth().getText();
		Round r = Round.valueOf(getJcbRound().getSelectedItem().toString());
		String statement = getJtfq().getText();
		HashMap<String, Boolean> hm = new HashMap<String, Boolean>();
		hm.put(getJtfa().getText(), getJcba().isSelected());
		hm.put(getJtfb().getText(), getJcbb().isSelected());
		hm.put(getJtfc().getText(), getJcbc().isSelected());
		hm.put(getJtfd().getText(), getJcbd().isSelected());
		Question q = new Question(author, r, statement, hm);
		return q;
	}
	
	/**
	 * Saves the question in the deck.
	 * 
	 */
	public void onSave()
	{
		if (editmode)
		{
			Deck.getInstance().getQuestions().set(this.getEditedRow(), getUpdatedQuestion());
			Serialisation.writeFile("Deck2.Json", Deck.getInstance().toJson());
			Keyword.getInstance().addKeyword(getJtfKeyword().getText(), Specialities.valueOf(getJcbSpeciality().getSelectedItem().toString()));
			Serialisation.writeFile("keyword.json", Keyword.getInstance().toJson());
			Deck.getInstance().notifyObs();
			String[] options = {"OK"};
			int input = JOptionPane.showOptionDialog(null, "This question was successfully updated.", "", JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options , options[0]);
			if(input == JOptionPane.OK_OPTION)
			{
				(SwingUtilities.getWindowAncestor(QuestionPanel.this)).dispose();
			}
		}
		else
		{
			try
			{
				if (!Deck.getInstance().isDuplicate(getUpdatedQuestion()))
				{
					Deck.getInstance().addQuestion(getUpdatedQuestion());
					Serialisation.writeFile("Deck2.Json", Deck.getInstance().toJson());
					Keyword.getInstance().addKeyword(getJtfKeyword().getText(), Specialities.valueOf(getJcbSpeciality().getSelectedItem().toString()));
					Serialisation.writeFile("keyword.json", Keyword.getInstance().toJson());
					Deck.getInstance().notifyObs();
					String[] options = {"OK"};
					int input = JOptionPane.showOptionDialog(null, "This question was successfully added to the deck.", "", JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options , options[0]);
					if(input == JOptionPane.OK_OPTION)
					{
						(SwingUtilities.getWindowAncestor(QuestionPanel.this)).dispose();
					}
				}
				
				else
				{
					JOptionPane.showMessageDialog(QuestionPanel.this,"This question already exists in this deck.");
				}
					
			}	
			catch(QuestionException exception)
			{
				exception.printStackTrace();
			} catch (CheckFailException e) {
				e.printStackTrace();
			} catch (MoreThan4ChoicesException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Cleans all the information from a question in the panel's component.
	 */
	public void clear()
	{
		this.getJtfauth().setText("");
		this.getJcbRound().setSelectedItem("FIRST_ROUND");
		this.getJtfq().setText("");
		this.getBg().clearSelection();
		this.getJtfa().setText("");
		this.getJtfb().setText("");
		this.getJtfc().setText("");
		this.getJtfd().setText("");
		
	}
	
	/**
	 * gets the button group of the 4 choices.
	 * @return the button group of the 4 choices.
	 */
	public ButtonGroup getBg() 
	{
		if (bg == null)
		{
			bg = new ButtonGroup();
			bg.add(this.getJcba());
			bg.add(this.getJcbb());
			bg.add(this.getJcbc());
			bg.add(this.getJcbd());
		}
		return bg;
	}
	
	/**
	 * gets the label author.
	 * @return the label author.
	 */
	public JLabel getJlauthor() {
		if (jlauthor == null)
		{
			jlauthor = new JLabel("Author :");
		}
		return jlauthor;
	}

	/**
	 * gets the label question.
	 * @return the label question.
	 */
	public JLabel getJlquestion() {
		if (jlquestion == null)
		{
			jlquestion = new JLabel("Question :");
		}
		return jlquestion;
	}
	
	/**
	 * gets the textfield of the statement.
	 * @return the textfield of the statement.
	 */
	public JTextField getJtfq() {
		if (jtfq == null)
		{
			jtfq = new JTextField(50);
		}
		return jtfq;
	}
	
	/**
	 * gets the textfield of the choice a.
	 * @return the textfield of the choice a.
	 */
	public JTextField getJtfa() {
		if (jtfa == null)
		{
			jtfa = new JTextField(15);
		}
		return jtfa;
	}
	
	/**
	 * gets the textfield of the choice b.
	 * @return the textfield of the choice b.
	 */
	public JTextField getJtfb() {
		if (jtfb == null)
		{
			jtfb = new JTextField(15);
		}
		return jtfb;
	}
	
	/**
	 * gets the textfield of the choice c.
	 * @return the textfield of the choice c.
	 */
	public JTextField getJtfc() {
		if (jtfc == null)
		{
			jtfc = new JTextField(15);
		}
		return jtfc;
	}
	
	/**
	 * gets the textfield of the choice d.
	 * @return the textfield of the choice d.
	 */
	public JTextField getJtfd() {
		if (jtfd == null)
		{
			jtfd = new JTextField(15);
		}
		return jtfd;
	}
	
	
	/**
	 * gets the index of the question edited.
	 * @return the index of the question edited.
	 */
	public int getEditedRow() {
		return editedRow;
	}
	
	
	/**
	 * gets the textfield author.
	 * @return the textfield author.
	 */
	public JTextField getJtfauth() {
		if (jtfauth == null)
		{
			jtfauth = new JTextField(10);
		}
		return jtfauth;
	}
	
	/**
	 * gets the label round.
	 * @return the label round.
	 */
	public JLabel getJlround() 
	{
		if (jlround == null)
		{
			jlround = new JLabel("Round :");
		}
		return jlround;
	}
	
	 /**
	 * gets the combo box round.
	 * @return the combo box round.
	 */
	public JComboBox<String> getJcbRound() 
	{
		if (jcbRound == null)
		{
			jcbRound = new JComboBox<String>();
			for(int i=0;i<Round.values().length;i++){
				jcbRound.addItem(Round.values()[i].name());
			}
		}
		return jcbRound;
	}
	
	
	/**
	 * gets the button save.
	 * @return the button save.
	 */
	public JButton getSave() 
	{
		if (save == null)
		{
			save = new JButton("Save");
			save.setPreferredSize(new Dimension(100,30));
			save.addActionListener(e->onSave());
		}
		return save;
	}
	
	/**
	 * gets the button cancel.
	 * @return the button cancel.
	 */
	public JButton getCancel() 
	{
		if (cancel == null)
		{
			cancel = new JButton("Clear");
			cancel.setPreferredSize(new Dimension(100,30));
			cancel.addActionListener(e->clear());
		}
		return cancel;
	}
	
	/**
	 * gets the button close.
	 * @return the button close.
	 */
	public JButton getClose() 
	{
		if (close == null)
		{
			close = new JButton("Close");
			close.setPreferredSize(new Dimension(100,30));
			close.addActionListener(e->{
				(SwingUtilities.getWindowAncestor(QuestionPanel.this)).dispose();
			});
		}
		return close;
	}
	
	/**
	 * gets the check box a.
	 * @return the check box a.
	 */
	public JCheckBox getJcba() 
	{
		if (jcba == null)
		{
			jcba = new JCheckBox();
		}
		return jcba;
	}
	
	/**
	 * gets the check box b.
	 * @return the check box b.
	 */
	public JCheckBox getJcbb() 
	{
		if (jcbb == null)
		{
			jcbb = new JCheckBox();
		}
		return jcbb;
	}
	
	/**
	 * gets the check box c.
	 * @return the check box c.
	 */
	public JCheckBox getJcbc()
	{
		if (jcbc == null)
		{
			jcbc = new JCheckBox();
		}
		return jcbc;
	}
	
	/**
	 * gets the check box d.
	 * @return the check box d.
	 */
	public JCheckBox getJcbd() 
	{
		if (jcbd == null)
		{
			jcbd = new JCheckBox();
		}
		return jcbd;
	}
	
	/**
	 * gets the label for the choice a.
	 * @return the label for the choice a.
	 */
	public JLabel getJla() 
	{
		if (jla == null)
		{
			jla = new JLabel("A :");
		}
		return jla;
	}

	/**
	 * gets the label for the choice b.
	 * @return the label for the choice b.
	 */
	public JLabel getJlb() 
	{
		if (jlb == null)
		{
			jlb = new JLabel("B :");
		}
		return jlb;
	}

	/**
	 * gets the label for the choice c.
	 * @return the label for the choice c.
	 */
	public JLabel getJlc() 
	{
		if (jlc == null)
		{
			jlc = new JLabel("C :");
		}
		return jlc;
	}

	/**
	 * gets the label for the choice d.
	 * @return the label for the choice d.
	 */
	public JLabel getJld() 
	{
		if (jld == null)
		{
			jld = new JLabel("D :");
		}
		return jld;
	}

	/**
	 * gets the label keyword.
	 * @return the label keyword.
	 */
	public JLabel getJlKeyword() {
		if(jlKeyword==null){
			jlKeyword=new JLabel("Keyword :");
		}
		return jlKeyword;
	}

	/**
	 * gets the label speciality.
	 * @return the label speciality.
	 */
	public JLabel getJlSpeciality() {
		if(jlSpeciality==null){
			jlSpeciality=new JLabel("Speciality :");
		}
		return jlSpeciality;
	}

	/**
	 * gets the text field keyword.
	 * @return the text field keyword.
	 */
	public JTextField getJtfKeyword() {
		if(jtfKeyword==null){
			jtfKeyword=new JTextField(15);
		}
		return jtfKeyword;
	}

	/**
	 * gets the combo box of specialities.
	 * @return the combo box of specialities.
	 */
	public JComboBox<String> getJcbSpeciality() {
		if (jcbSpeciality == null){
			jcbSpeciality = new JComboBox<String>();
			for(int i=0;i<Specialities.values().length;i++){
				jcbSpeciality.addItem(Specialities.values()[i].name());
			}
		}
		return jcbSpeciality;
	}
}
