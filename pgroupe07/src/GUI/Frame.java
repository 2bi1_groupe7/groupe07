package GUI;

import java.awt.CardLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import model.UserList;
import util.Password;
import util.Serialisation;

/**
 * This class initializes the frame and each panel of the application.
 * 
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
public class Frame extends JFrame
{
	static int Screenwidth = Toolkit.getDefaultToolkit().getScreenSize().width;
	static int Screenheight = Toolkit.getDefaultToolkit().getScreenSize().height;
	
	private CardLayout layout;

	private LoginPanel logpanel;
	private GamePanel gamepanel;
	private AdminPanel adminpanel;
	private ScorePanel scorepanel;

	private JMenuBar menubar;
	private JMenu menu, menuoptions, menuhelp;
	private JMenuItem jmirestart, jmiexit, jmirules, jmiabout, jmiscores;
	private boolean connected = false;

	
	/**
	 * default constructor.
	 */
	public Frame()
	{
		/* Menu */
		this.getMenu().add(this.getJmirestart());
		this.getMenu().add(this.getJmiscores());
		this.getMenu().addSeparator();
		this.getMenu().add(this.getJmiexit());
		

		this.getMenuhelp().add(this.getJmirules());
		this.getMenuhelp().addSeparator();
		this.getMenuhelp().add(this.getJmiabout());
		
		this.getMenubar().add(this.getMenu());
		this.getMenubar().add(this.getMenuhelp());
		
		this.setJMenuBar(this.getMenubar());
			
		/* ENTER Button */
		this.getRootPane().setDefaultButton(this.getLogpanel().getJb());
		 		
		/* CardLayout */
		layout = new CardLayout();
		this.getContentPane().setLayout(layout);
		this.getContentPane().add(this.getLogpanel(), "LOGIN");
		this.getContentPane().add(this.getGamepanel(), "GAME");
		this.getContentPane().add(this.getAdminpanel(), "ADMIN");
		this.getContentPane().add(this.getScorepanel(), "SCORE");
		layout.show(this.getContentPane(), "LOGIN");
	
		/* Options Frame */
		this.setSize(1280, 720);
		this.setTitle("Who Wants to Catch 'Em All ?");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setVisible(true);
		
		ImageIcon icon = new ImageIcon("img/pokemon-icon.PNG");
		this.setIconImage(icon.getImage());
	}

	/* (non-Javadoc)
	 * @see java.awt.Container#getLayout()
	 */
	public CardLayout getLayout() {
		if(layout==null){
			layout = new CardLayout();
		}
		return layout;
	}

	/**
	 * gets the menu bar of the frame.
	 * @return the menu bar of the frame.
	 */
	public JMenuBar getMenubar() 
	{
		if (menubar == null)
		{
			menubar = new JMenuBar();
		}
		return menubar;
	}
	
	/**
	 * gets the panel administration.
	 * @return the panel administration.
	 */
	public AdminPanel getAdminpanel() 
	{
		if (adminpanel == null)
		{
			adminpanel = new AdminPanel();
		}
		return adminpanel;
	}
	
	/**
	 * gets the menu.
	 * @return the menu.
	 */
	public JMenu getMenu() 
	{
		if (menu == null)
		{
			menu = new JMenu("Game");
		}
		return menu;
	}
	
	/**
	 * sets the boolean true if we are logged, else false.
	 * @param connected true if we are logged, else false.
	 */
	public void setConnected(boolean connected) 
	{
		this.connected = connected;
	}
	
	/**
	 * gets the menu options.
	 * @return the menu options.
	 */
	public JMenu getMenuoptions() 
	{
		if (menuoptions == null)
		{
			menuoptions = new JMenu("Options");
		}
		return menuoptions;
	}
	
	/**
	 * gets the ScorePanel.
	 * @return the ScorePanel.
	 */
	public ScorePanel getScorepanel() 
	{
		if (scorepanel == null)
		{
			scorepanel = new ScorePanel();
		}
		return scorepanel;
	}
	
	/**
	 * gets the menu item Score.
	 * @return the menu item score.
	 */
	public JMenuItem getJmiscores() 
	{
		if (jmiscores == null){
			jmiscores = new JMenuItem("Scoreboard");
			jmiscores.addActionListener(e -> {
				layout.show(this.getContentPane(), "SCORE");
			});
		}
		return jmiscores;
	}

	/**
	 * gets the menu item restart.
	 * @return the menu item restart.
	 */
	public JMenuItem getJmirestart() 
	{
		if (jmirestart == null)
		{
			jmirestart = new JMenuItem("New Game");
			jmirestart.addActionListener(e-> 
				{
					if (connected)
					{
						Frame.this.getGamepanel().gameLaunch();
						Frame.this.getGamepanel().nextInterface();
						layout.show(Frame.this.getContentPane(), "GAME");
					}
				}
				
			);
		}
		return jmirestart;
	}

	/**
	 * gets the logpanel.
	 * @return the logpanel.
	 */
	public LoginPanel getLogpanel() 
	{
		if (logpanel == null)
		{
			logpanel = new LoginPanel();
			logpanel.getJb().addActionListener(new ActionListener() 
			{
	            public void actionPerformed(ActionEvent e)
	            {
	            	String name = getLogpanel().getJtfname().getText();
					String password = new String(getLogpanel().getJpf().getPassword());
					String warning = "";
					if (name.equals("") || password.equals(""))
					{
						if (name.equals(""))
						{
							warning = warning +"Please enter a username.\n";
						}
						if (password.equals(""))
						{
							warning = warning +"Please enter a password.";
						}
						JOptionPane.showMessageDialog(Frame.this, warning);
	            	}
					else
					{
						boolean found = false;
						for(int i = 0; i < UserList.getInstance().getUserlist().size(); i++)
						{
							String listname = UserList.getInstance().getUserlist().get(i).getName();
							String listpasswd = UserList.getInstance().getUserlist().get(i).getPassword();
							if (listname.equals(name.toLowerCase()))
							{
								found = true;
								if (Password.checkPassword(password, listpasswd))
								{
									if (name.toLowerCase().equals("user"))
									{
										connected = true;
										layout.show(Frame.this.getContentPane(), "GAME");
									}
									else
										layout.show(Frame.this.getContentPane(), "ADMIN");
									logpanel.getJtfname().setText("");
									logpanel.getJpf().setText("");
								}
								else
									JOptionPane.showMessageDialog(Frame.this, "Wrong password for user \""+name+"\".");
							}
						}
						if (!found)
							JOptionPane.showMessageDialog(Frame.this, "User not found.");
					}
	            }
	        }); 
		}
		return logpanel;
	}

	/**
	 * gets the gamepanel.
	 * @return the gamepanel.
	 */
	public GamePanel getGamepanel()
	{
		if (gamepanel == null)
		{
			gamepanel = new GamePanel();
		}
		return gamepanel;
	}
	
	/**
	 * gets the menu Exit.
	 * @return the menu Exit.
	 */
	public JMenuItem getJmiexit() 
	{
		if (jmiexit == null)
		{
			jmiexit = new JMenuItem("Exit");
			jmiexit.addActionListener(e->System.exit(0));
		}
		return jmiexit;
	}
	
	/**
	 * returns the value of the boolean connected.
	 * @return true is we are log in, else false;
	 */
	public boolean isConnected() 
	{
		return connected;
	}
	
	/**
	 * gets the menu item Rules.
	 * @return the menu item rules.
	 */
	public JMenuItem getJmirules() 
	{
		if (jmirules == null)
		{
			jmirules = new JMenuItem("Game rules");
			jmirules.addActionListener(e-> {
					String text = " Who Wants To Catch 'Em All is based on the TV show \"Who Wants To Be A Millionaire\" \n"
							+ " and the media franchise \"Pok�mon\" .\n"
							+ " You must select the right answer to fifteen questions in a row to catch the legendary Pok�mon Mewtwo.\n"
							+ " Each question has four possible answers but only one is correct.\n"
							+ " Questions are getting harder as you progress but you get two safe catches (Pikachu and Alakazam) : \n if you reach them, you are guaranteed to leave with them.\n"
							+ " You can also quit the game at any time and take the current Pok�mon you have.\n"
							+ " You get three lifelines to help you answer :\n"
							+ " � 50/50 : eliminates two wrong answers.\n"
							+ " � Phone a Friend : let\'s a specialist friend tell you what he believes is the right answer.\n"
							+ " � Ask the Audience : let\'s the audience tell you what they believe is the right answer.";
					JTextArea textArea = new JTextArea(13,55);
					textArea.setText(text);
					textArea.setEditable(false);
					JScrollPane jsp = new JScrollPane(textArea);
					JOptionPane.showMessageDialog(Frame.this, jsp, "Rules for \"Who Wants to Catch 'Em All ?\"", JOptionPane.PLAIN_MESSAGE);
				}
					
			);
		}
		return jmirules;
	}
	
	/**
	 * gets the menu item help.
	 * @return the menu item help.
	 */
	public JMenu getMenuhelp() 
	{
		if (menuhelp == null)
		{
			menuhelp = new JMenu("Help");
		}
		return menuhelp;
	}
	
	/**
	 * gets the menu item about.
	 * @return the menu item about.
	 */
	public JMenuItem getJmiabout() 
	{
		if (jmiabout == null)
		{
			jmiabout = new JMenuItem("About...");
			ImageIcon train = new ImageIcon("img/trainer.png");
			jmiabout.addActionListener(e->{
				JOptionPane.showMessageDialog(Frame.this,
					    "<html><u>Development team :</u></html>\n"
					    + "� De Cnuydt Benjamin\n"
					    + "� Franco Maxime\n"
					    + "� Lipani Corrado\n\n"
					    + "<html><u>Product owner :</u></html>\n"
					    + "� Mr. Altares", "About",JOptionPane.INFORMATION_MESSAGE,train);
			});
	
		}
		return jmiabout;
	}
		
}