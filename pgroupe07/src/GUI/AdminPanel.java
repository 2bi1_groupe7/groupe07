package GUI;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * This class represents the panel administration
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
public class AdminPanel extends JPanel{
	private JTabbedPane jtpAdmin;
	private DeckPanel dpAdmin;
	private KeywordPanel kpAdmin;
	

	/**
	 * default constructor.
	 */
	public AdminPanel() {
		super();
		this.setLayout(new BorderLayout());
		this.setBackground(Color.WHITE);
		this.add(getJtpAdmin());
	}

	/**
	 * gets the panel Administration.
	 * @return the panel Administration.
	 */
	public JTabbedPane getJtpAdmin() {
		if(jtpAdmin==null){
			jtpAdmin = new JTabbedPane();
			jtpAdmin.add("Deck Administration",this.getDpAdmin());
			jtpAdmin.setBackgroundAt(0, Color.GRAY);
			jtpAdmin.add("Keyword Administration",this.getKpAdmin());
			jtpAdmin.setBackgroundAt(1, Color.GRAY);
		}
		return jtpAdmin;
	}

	/**
	 * gets the panel administration Deck.
	 * @return the panel administration deck.
	 */
	public DeckPanel getDpAdmin() {
		if(dpAdmin==null){
			dpAdmin=new DeckPanel();
		}
		return dpAdmin;
	}
	
	/**
	 * gets the panel keyword.
	 * @return the panel keyword.
	 */
	public KeywordPanel getKpAdmin(){
		if(kpAdmin==null){
			kpAdmin=new KeywordPanel();
		}
		return kpAdmin;
	}
}
