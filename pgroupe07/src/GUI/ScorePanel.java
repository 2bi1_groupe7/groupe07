package GUI;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import model.Observer;
import model.ScoreList;

/**
 * this class represent the Score panel of the application.
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
public class ScorePanel extends JPanel implements Observer
{
	private JTable jtable;
	private TableScoreModel tm;
	private JLabel title, back;

	
	/**
	 * default constructor
	 */
	public ScorePanel()
	{
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10,0,10,0);
		c.gridx = 0;
		c.gridy = 0;
		ImageIcon imgicon = new ImageIcon("img/scoreboardtitle.png");
		JLabel title = new JLabel(imgicon);
		this.add(title, c);
		JScrollPane spTable = new JScrollPane(getJtable());
		spTable.setBackground(new Color(0,0,0,50));
		spTable.getViewport().setBackground(new Color(0,0,0,50));
		c.gridx = 0;
		c.gridy = 1;
		this.add(spTable, c);
		c.gridx = 0;
		c.gridy = 2;
		this.add(this.getBack(), c);
		ScoreList.getInstance().addObs(this);
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Image img;
		try 
		{
			img = ImageIO.read(new File("img/BGscoreboard.jpg"));
		     g.drawImage(img, 0, 0, this);

		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		this.repaint();
	}
	
	/**
	 * gets the JLabel title.
	 * @return the JLabel title.
	 */
	public JLabel getTitle() 
	{
		if (title == null)
		{
			title = new JLabel("SCOREBOARD");
			title.setFont(new Font("Impact", Font.PLAIN, 50));
			title.setForeground(Color.black);
		}
		return title;
	}
	
	/**
	 * gets the JLabel back.
	 * @return the JLabel back.
	 */
	public JLabel getBack() 
	{
		if (back == null)
		{
			back = new JLabel(new ImageIcon("img/backbutton.png"));
			back.addMouseListener(new MouseListener(){

				@Override
				public void mouseClicked(MouseEvent arg0) 
				{
					Frame f =((Frame)SwingUtilities.getWindowAncestor(ScorePanel.this));
					if (f.isConnected())
					{
						Container cont = f.getContentPane();
						((CardLayout)cont.getLayout()).show(cont, "GAME");
					}
					else
					{
						Container cont = f.getContentPane();
						((CardLayout)cont.getLayout()).show(cont, "LOGIN");
					}
				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mousePressed(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			
		}
		return back;
	}

	/**
	 * gets the JTable of scores.
	 * @return the JTable of scores.
	 */
	public JTable getJtable() 
	{
		if (jtable == null)
		{
			jtable = new JTable(this.getTm());
			jtable.setRowSelectionAllowed(false);
			jtable.setColumnSelectionAllowed(false);
			jtable.setBackground(new Color(0,0,0,50));
			jtable.getTableHeader().setBackground(new Color(119,24,21,255));
			jtable.getTableHeader().setForeground(Color.WHITE);
			jtable.getTableHeader().setReorderingAllowed(false);
			Color color = UIManager.getColor("Table.gridColor");
			MatteBorder border = new MatteBorder(1, 1, 0, 0, color);
			jtable.setBorder(border);
			jtable.setForeground(Color.white);
			TableColumn dates = jtable.getColumn("Date");
			dates.setMinWidth(100);
			jtable.setEnabled(false);
		}
		return jtable;
	}
	/**
	 * gets the TableScoreModel of the table Score.
	 * @return the TableScoreModel of the table Score.
	 */
	public TableScoreModel getTm() 
	{
		if (tm == null)
		{
			tm = new TableScoreModel();
		}
		return tm;
	}

	/* (non-Javadoc)
	 * @see model.Observer#update()
	 */
	@Override
	public void update() 
	{
		this.getTm().fireTableDataChanged();
	}
}

/**
 * this class gives the representation for the score table.
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
class TableScoreModel extends AbstractTableModel
{
	private ScoreList s = ScoreList.getInstance();
	private final String[] entetes = { "Name", "Questions", "Pokemon","Date"};

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() 
	{
		
		return entetes.length;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() 
	{
		return s.getScores().size();
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int rowIndex, int columnIndex) 
	{
		switch(columnIndex)
		{
			case 0:
				return s.getScores().get(rowIndex).getName();	
			case 1:
				return s.getScores().get(rowIndex).getQuestions();
			case 2:
				return s.getScores().get(rowIndex).getPokemon();
			case 3:
				return s.getScores().get(rowIndex).getDatetime();
			default:
				return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int column)
	{
		return entetes[column];
	}
	
}