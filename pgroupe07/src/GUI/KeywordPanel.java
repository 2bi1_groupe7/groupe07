package GUI;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import model.Keyword;
import model.Observer;
import model.Specialities;
import util.Serialisation;


/**
 * the panel administration keyword.
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
public class KeywordPanel extends JPanel implements Observer {
	private JTable jtable;
	private JButton delete, edit, add, back;
	private TableModelKeyword tm;
	private Keyword k=Keyword.getInstance();
	
	/**
	 * default constructor
	 */
	public KeywordPanel(){
		this.setLayout(new BorderLayout());
		JScrollPane spTable = new JScrollPane(getJtable());
		this.add(spTable, BorderLayout.CENTER);
		spTable.setBackground(Color.GRAY);
		spTable.getViewport().setBackground(Color.GRAY);
		JPanel jp = new JPanel();
		jp.setLayout(new FlowLayout());
		jp.add(this.getAdd());
		jp.add(this.getEdit());
		jp.add(this.getDelete());
		jp.add(this.getBack());
		this.add(jp, BorderLayout.NORTH);
		jp.setBackground(Color.GRAY);
		Keyword.getInstance().addObs(this);
	}

	/**
	 * gets the table of keyword.
	 * @return the table of keyword.
	 */
	public JTable getJtable() 
	{
		if (jtable == null)
		{
			jtable = new JTable(this.getTableModelKeyword());
			jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			jtable.setCellSelectionEnabled(true);
			jtable.getTableHeader().setReorderingAllowed(false);
		}
		return jtable;
	}
	
	/**
	 * gets the table model for the keyword.
	 * @return the table model for the keyword.
	 */
	public TableModelKeyword getTableModelKeyword()
	{
		if (tm == null)
		{
			tm = new TableModelKeyword();
		}
		return tm;
	}
	
	/**
	 * this method is called when someone edits a keyword.
	 */
	public void onEdit(){
		int row = this.getJtable().getSelectedRow();
		int column = this.getJtable().getSelectedColumn();
		String tmp=k.get(row, column);
		if (row != -1) {
			JFrame addFrame = new JFrame();
			addFrame.setSize(900, 300);
			addFrame.add(new WordPanel(tmp,row,column));
			addFrame.setVisible(true);
			addFrame.setTitle("Word Preview");
			addFrame.setLocationRelativeTo(null);
		}
	}
	
	/**
	 * this method is called when someone deletes a keyword.
	 */
	public void onDelete()
	{
		int row = this.getJtable().getSelectedRow();
		int column=this.getJtable().getSelectedColumn();
		if (row != -1)
			this.getTableModelKeyword().removeRow(column,row);
	}
	
	/**
	 * this method is called when someone adds a keyword.
	 */
	public void onAdd()
	{
		JFrame addFrame = new JFrame();
		addFrame.setSize(900, 300);
		addFrame.add(new WordPanel());
		addFrame.setVisible(true);
		addFrame.setTitle("Keyword Preview");
		addFrame.setLocationRelativeTo(null);
	}
	
	/**
	 * gets the button delete.
	 * @return the button delete.
	 */
	public JButton getDelete() 
	{
		if (delete == null)
		{
			delete = new JButton("Delete");
			delete.setPreferredSize(new Dimension(100,30));
			delete.addActionListener(e->onDelete());
		}
		return delete;
	}

	/**
	 * gets the button edit.
	 * @return the button edit.
	 */
	public JButton getEdit() 
	{
		if (edit == null)
		{
			edit = new JButton("Edit");
			edit.setPreferredSize(new Dimension(100,30));
			edit.addActionListener(e->onEdit());
		}
		return edit;
	}
	
	/**
	 * gets the button add.
	 * @return the button add.
	 */
	public JButton getAdd() 
	{
		if (add == null)
		{
			add = new JButton("Add");
			add.setPreferredSize(new Dimension(100,30));
			add.addActionListener(e->onAdd());
		}
		return add;
	}
	
	/**
	 * gets the button back.
	 * @return the button back.
	 */
	public JButton getBack() 
	{
		if (back == null)
		{
			back = new JButton("Back");
			back.setPreferredSize(new Dimension(100,30));
			back.setBackground(Color.DARK_GRAY);
			back.setForeground(Color.WHITE);
			back.addActionListener(e->{
				Container cont = ((Frame)SwingUtilities.getWindowAncestor(KeywordPanel.this)).getContentPane();
				((CardLayout)cont.getLayout()).show(cont, "LOGIN");
			});
		}
		return back;
	}
	
	/* (non-Javadoc)
	 * @see model.Observer#update()
	 */
	public void update() 
	{
		this.getTableModelKeyword().fireTableDataChanged();
	}

}
/**
 * this class gives the representation for the table of keyword.
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
class TableModelKeyword extends AbstractTableModel
{
	private Keyword k = Keyword.getInstance();
	private final String[] entetes = new String[Specialities.values().length];
	
	/**
	 * default constructor
	 */
	public TableModelKeyword()
	{
		super();
		for(int i=0; i<Specialities.values().length;i++){
			entetes[i]=Specialities.values()[i].toString();
		}
	}
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	public int getRowCount() 
	{
		return k.getSize();
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.AbstractTableModel#getColumnName(int)
	 */
	public String getColumnName(int column)
	{
		return entetes[column];
	}
	
	/**
	 * gets the class keyword.
	 * @return the class Keyword.
	 */
	public Keyword getKeyword()
	{
		return k;
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	public int getColumnCount() 
	{
		return entetes.length;
	}
	
	/**
	 * removes a keyword from the list of Keyword.
	 * @param row the index of the keyword.
	 * @param column the index of the speciality of the keyword.
	 */
	public void removeRow(int row,int column)
    {
		k.delKeyword(row, column);
		Serialisation.writeFile("keyword.json", k.toJson());
        fireTableRowsDeleted(row, row);
    }

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int row, int column) 
	{
		return k.get(row, column);
	}
}

/**
 * the panel Word panel.
 * @author De Cnuydt Benjamin Franco Maxime Lipani Corrado
 *
 */
class WordPanel extends JPanel{
	Keyword k=Keyword.getInstance();
	JLabel jlWord,jlSpeciality;
	JTextField jtfWord;
	JComboBox<String>jcbSpeciality;
	JButton jbsave,jbclear,jbclose;
	private int editedRow,editedColumn;
	public boolean editmode=false;
	
	
	/**
	 * the constructor WordPanel with all the information to set in the frame.
	 * @param word the keyword.
	 * @param editedRow the index of the keyword.
	 * @param editedColumn the speciality of the keyword.
	 */
	public WordPanel(String word,int editedRow,int editedColumn){
		this.editedColumn=editedColumn;
		this.editedRow=editedRow;
		initUI();
		getJtfWord().setText(word);
		jcbSpeciality.setSelectedItem(Specialities.values()[editedColumn].name());
		editmode=true;
	}
	
	/**
	 * default  constructor
	 */
	public WordPanel(){
		initUI();
	}
	
	/**
	 * sets all the information in the panel.
	 */
	public void initUI(){
		this.setLayout(new GridBagLayout());
		JPanel center = new JPanel();
		this.add(center);
		GroupLayout gl = new GroupLayout(center);
		center.setLayout(gl);
		gl.setAutoCreateGaps(true);
		gl.setAutoCreateContainerGaps(true);
		
		gl.setHorizontalGroup(gl.createSequentialGroup()
				.addComponent(this.getJlWord(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addComponent(this.getJtfWord(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addComponent(this.getJlSpeciality(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addComponent(this.getJcbSpeciality(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbsave(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(getJbcancel(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(getJbclose(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
				)
		);
		
		gl.setVerticalGroup(gl.createParallelGroup()
				.addComponent(this.getJlWord(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addComponent(this.getJtfWord(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addComponent(this.getJlSpeciality(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addComponent(this.getJcbSpeciality(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
						GroupLayout.PREFERRED_SIZE)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbsave(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(getJbcancel(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(getJbclose(),GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
				)
		);
	}
	
	/**
	 * cleans all the information in the panel.
	 */
	public void clear()
	{
		this.getJtfWord().setText("");
		this.getJcbSpeciality().setSelectedItem(Specialities.values()[0].name());
		
	}
	
	/**
	 * this method is called when someone saves a keyword.
	 */
	public void onSave()
	{
		if (editmode)
		{
			System.out.println( editedColumn+"        "+ editedRow);
			k.updateKeyword(getJtfWord().getText(), editedColumn, editedRow);
			Serialisation.writeFile("keyword.json", Keyword.getInstance().toJson());
			Keyword.getInstance().notifyObs();
			String[] options = {"OK"};
			int input = JOptionPane.showOptionDialog(null, "This keyword was successfully updated.", "", JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options , options[0]);
			if(input == JOptionPane.OK_OPTION)
			{
				(SwingUtilities.getWindowAncestor(WordPanel.this)).dispose();
			}
		}
		else
		{
			if(!k.contains(getJtfWord().getText(),Specialities.values()[getJcbSpeciality().getSelectedIndex()])){
				Keyword.getInstance().addKeyword(getJtfWord().getText(), Specialities.valueOf(getJcbSpeciality().getSelectedItem().toString()));
				Serialisation.writeFile("keyword.json", Keyword.getInstance().toJson());
				Keyword.getInstance().notifyObs();
				String[] options = {"OK"};
				int input = JOptionPane.showOptionDialog(null, "This keyword was successfully added to the deck.", "", JOptionPane.OK_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options , options[0]);
				if(input == JOptionPane.OK_OPTION)
				{
					(SwingUtilities.getWindowAncestor(WordPanel.this)).dispose();
				}
			}

			else
			{
				JOptionPane.showMessageDialog(WordPanel.this,"This keyword already exists in this deck.");
			}
		}
	}
	
	/**
	 * gets the JLabel word.
	 * @return the JLabel word.
	 */
	public JLabel getJlWord(){
		if(jlWord==null){
			jlWord=new JLabel("Keyword :");
		}	
		return jlWord;
	}
	
	/**
	 * gets the JLabel speciality.
	 * @return the JLabel speciality.
	 */
	public JLabel getJlSpeciality() {
		if(jlSpeciality==null){
			jlSpeciality=new JLabel("Speciality :");
		}
		return jlSpeciality;
	}
	
	/**
	 * gets the JTextField word.
	 * @return the JTextField word.
	 */
	public JTextField getJtfWord() {
		if(jtfWord==null){
			jtfWord=new JTextField(15);
		}
		return jtfWord;
	}
	
	/**
	 * gets the JComboBox speciality.
	 * @return JComboBox speciality.
	 */
	public JComboBox<String> getJcbSpeciality() {
		if (jcbSpeciality == null){
			jcbSpeciality = new JComboBox<String>();
			for(int i=0;i<Specialities.values().length;i++){
				jcbSpeciality.addItem(Specialities.values()[i].name());
			}
		}
		return jcbSpeciality;
	}
	
	/**
	 * gets the JButton save.
	 * @return the JButton save.
	 */
	public JButton getJbsave() 
	{
		if (jbsave == null)
		{
			jbsave = new JButton("Save");
			jbsave.setPreferredSize(new Dimension(100,30));
			jbsave.addActionListener(e->onSave());
		}
		return jbsave;
	}
	
	/** 
	 * gets the JButton cancel.
	 * @return the JButton cancel.
	 */
	public JButton getJbcancel() 
	{
		if (jbclear == null)
		{
			jbclear= new JButton("Clear");
			jbclear.setPreferredSize(new Dimension(100,30));
			jbclear.addActionListener(e->clear());
		}
		return jbclear;
	}
	
	
	/**
	 * gets the JButton close.
	 * @return the JButton close.
	 */
	public JButton getJbclose() 
	{
		if (jbclose == null)
		{
			jbclose = new JButton("Close");
			jbclose.setPreferredSize(new Dimension(100,30));
			jbclose.addActionListener(e->{
				(SwingUtilities.getWindowAncestor(WordPanel.this)).dispose();
			});
		}
		return jbclose;
	}
}