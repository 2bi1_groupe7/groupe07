package exceptions;

/**
 * MoreThan4ChoicesException is thrown when a question has more than 4 choices.
 * @author De Cnuydt Benjamin
 *
 */
public class MoreThan4ChoicesException extends Exception{
	
	/**
	 * Constructs a new exception.
	 * 
	 */
	public MoreThan4ChoicesException(){
		super("Question already has 4 choices.");
	}

}
