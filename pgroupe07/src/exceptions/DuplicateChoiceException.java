package exceptions;

/**
 * DuplicateChoiceException is thrown when  2 questions are equal. 
 * 
 * @author De Cnuydt benjamin
 *
 */
public class DuplicateChoiceException extends Exception{
	
	/**
	 * Constructs a new exception.
	 * 
	 */
	public DuplicateChoiceException(){
		super("Le choice already exists for the question.");
	}

}
