package exceptions;

/**
 * QuestionException is thrown when a problem about the question occurs in the deck.
 * @author De Cnuydt Benjamin
 *
 */
public class QuestionException extends Exception
{

	private static final long serialVersionUID = 1L;

	public QuestionException() 
	{
		super("Question problem in the deck.");
	}
}