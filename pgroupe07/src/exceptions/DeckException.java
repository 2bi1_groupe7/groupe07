package exceptions;

public class DeckException extends Exception
{
	/**
	 * Exception thrown by Deck.
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructs a new exception with the specified detailed message.
	 * 
	 * @param message	the detailed message.
	 */
	public DeckException(String message) 
	{
		super(message);
	}
}
