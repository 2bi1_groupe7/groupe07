package exceptions;

import model.Question;

/**
 * CheckFailException is thrown when a question is in a wrong format
 * 
 * @author Franco Maxime
 *
 */
public class CheckFailException extends Exception{
	
	public CheckFailException(Question q){
		super("The question is in a wrong format : "+q);
	}

}
