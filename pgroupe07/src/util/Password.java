package util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * the class Password is used for hashing a password.
 * @author Lipani Corrado
 *
 */
public class Password 
{

	/**
	 * gets a salted password from a password.
	 * @param passwd the password.
	 * @param salt the salt.
	 * @return a salted password.
	 */
	public static String getSaltedHash(String passwd, String salt) // Using SHA256
	{
		String salted = passwd + salt;
		byte[] data = new byte[0];
		try 
		{
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(salted.getBytes());
			data = md.digest();
			
		} 
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return salt+"$"+ByteToHexString(data);
	}
	
	/**
	 * gets a generated salt.
	 * @return a generated salt.
	 */
	public static String getGeneratedSalt()
	{
		Random r = new SecureRandom();
		byte[] salt = new byte[16];
		r.nextBytes(salt);
		return ByteToHexString(salt);
	}
	
	/**
	 * transforms a table of byte to a String
	 * @param b the table of byte
	 * @return the string value of the table of byte.
	 */
	public static String ByteToHexString(byte[] b)
	{
		StringBuffer hexString = new StringBuffer();

        for (int i = 0; i < b.length; i++) 
        {
            String hex = Integer.toHexString(0xff & b[i]);
            if(hex.length() == 1)
            	hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
	}
	/**
	 * checks if the password entered is correct.
	 * @param passwd the password entered.
	 * @param hash the hash of the real password.
	 * @return true if the password is good, else false.
	 */
	public static boolean checkPassword(String passwd, String hash)
	{
		String salt = hash.substring(0, hash.indexOf("$"));
		String hashPasswd = getSaltedHash(passwd, salt);
		if (hashPasswd.equals(hash))
			return true;
		return false;
	}
}
