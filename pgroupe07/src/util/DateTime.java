package util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class gives a representation of the current date.
 * @author Corrado Lipani
 *
 */
public class DateTime 
{
	/**
	 * gets the current date.
	 * @return the current date;
	 */
	public static String getCurrentDateTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date); 
	}
}
