package util;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * This class implements the serialization for the project
 * @author Franco Maxime
 *
 */
public class Serialisation 
{
	/**
	 * Reads a file.
	 * @param nomF the name of the file.
	 * @return the content of the file.
	 */
	public static String readFile(String nomF){
		String value = "";
		try (Scanner reader = new Scanner(new FileReader(nomF))) {
			while(reader.hasNextLine()){
				value+="\n"+reader.nextLine();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return value;
	}
	
	
	/**
	 * Writes a file
	 * @param fileName the name of the file.
	 * @param value the content of the file.
	 */
	public static void writeFile(String fileName, String value)
	{
		PrintWriter writer = null;
		try{
			FileWriter fw = new FileWriter(fileName);
			writer = new PrintWriter(fw);
			writer.println(value);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		finally {
			if(writer != null){
				writer.close();
			}
		}
	}
}
